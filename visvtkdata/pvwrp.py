# -*- coding: utf-8 -*-
"""Module containing Paraview wrapper functions.
"""

"""
Todo:
* ...
"""

"""
created  by: Nicola Scholl
created  on: 09/2015

modified by: Henning Schippke
modified on: 03.02.2017
"""

import paraview.simple as pv

# ===================================================================
# ===================================================================


def CreateBasicViewAndObject(loadPath, savePath, Area, Plotsettings):
    """Create basic RenderViewObject and show "surface with edges" representation
    of mesh.


    Parameters
    ----------

    loadPath : string
        Pfad unter dem die vtu-Dateien abgelegt sind.

    savePath : string
        Pfad unter dem die png-Bilder abgespeichert werden sollen.

    Area
        Name der vtu-Datei (ohne Endung) in der sich die Flaechendaten bzw.
        Daten zur Geometrie befinden.

    Plotsettings
        Ein mit der Klasse CreatePlotSettings erzeugtes Objekt.
        Die Attribute sind die Einstellungen fuer die spaeteren
        Ausgabe.


    Returns
    -------

    renderView
        Enthaelt Einstellungen zu der Kamera und zu der Pixel-Groesse des Bildes.

    inputData
        Enthaelt Daten ueber die Geometrie (Paraview).

    inputDataDisplay
        Enthaelt Daten zu der Darstellung der Geometrie (Paraview).


    Usage
    -----
    renderView, inputData, inputDataDisplay = CreateBasicViewAndObject(loadPath, savePath, Area, Plotsettings)
    """


    # Einlesen Daten
    inputData, filename = ReadData(loadPath,Area)

    # Darstellungsbereich erzeugen
    renderView = pv.GetActiveViewOrCreate('RenderView')

    from myParaviewModules.basics import SetRenderViewSettings
    SetRenderViewSettings(renderView,
                          viewSize     = Plotsettings.viewSize,
                          camPos       = Plotsettings.camPos,
                          camFocPoint  = Plotsettings.camFocPoint,
                          camClipRange = Plotsettings.camClipRange,
                          camParScal   = Plotsettings.camParScal)

    # Erzeugen der Geometrie-Darstellung
    inputDataDisplay = pv.GetDisplayProperties(inputData, view=renderView)

    # Darstellung der Elementgrenzen
    inputDataDisplay.SetRepresentationType('Surface With Edges')
    inputDataDisplay.EdgeColor = [0.0, 0.0, 0.0] # [0.0, 0.0, 0.0] = schwarz

    # Aendern der "Hintergrundfarbe" der Darstellungsfläche
    inputDataDisplay.AmbientColor = [1.0, 1.0, 1.0] # [1.0, 1.0, 1.0] = weiß

    # Einstellen der Linienstaerke
    inputDataDisplay.LineWidth = Plotsettings.lineWidth

    # Einstellen der Achsen an der Geometrie (Skalieren nicht moeglich)
    SetCubeAxesSettings(inputDataDisplay,
                        Visibility               = Plotsettings.showCubeAxes,
                        XTitle                   = 'x [m]',
                        YTitle                   = 'y [m]',
                        XAxisMinorTickVisibility = 0,
                        YAxisMinorTickVisibility = 0,
                        ZAxisVisibility          = 0)


    return renderView, inputData, inputDataDisplay

# ===================================================================
# ===================================================================


def ReadData(loadPath,filenameFull):

    import os

    filename, file_extension = os.path.splitext(filenameFull)

    if file_extension == '.vtu':
        inputData = pv.XMLUnstructuredGridReader(FileName=[loadPath + filenameFull])

    elif file_extension == '.xmf':
        inputData = pv.XDMFReader(FileNames=loadPath + filenameFull)

    else:
        print('Endung fehlt oder ist unzulaessig!')

    return inputData, filename


# ===================================================================
# ===================================================================


def SetCubeAxesSettings(DataDisplay, \
                        Visibility = 1, \
                        Color =  [0.0, 0.0, 0.0], \
                        TickLocation = 'Inside', \
                        XAxisVisibility = 1, \
                        YAxisVisibility = 1, \
                        ZAxisVisibility = 1, \
                        XTitle = 'X-Axis', \
                        YTitle = 'Y-Axis', \
                        ZTitle = 'Z-Axis', \
                        XAxisTickVisibility = 1, \
                        YAxisTickVisibility = 1, \
                        ZAxisTickVisibility = 1, \
                        XAxisMinorTickVisibility = 1, \
                        YAxisMinorTickVisibility = 1, \
                        ZAxisMinorTickVisibility = 1, \
                        XTitleDisplay = 1, \
                        YTitleDisplay = 1, \
                        ZTitleDisplay = 1, \
                        GridLineLocation = 'All Faces'):

    # Sichtbarmachen der Koordinatenachsen
    DataDisplay.CubeAxesVisibility = Visibility

    # Achesenfarbe zu z.B. Schwarz setzen
    DataDisplay.CubeAxesColor = Color

    DataDisplay.CubeAxesTickLocation	=	TickLocation

    # Einschalten der einzelnen Achsen
    DataDisplay.CubeAxesXAxisVisibility	=	XAxisVisibility
    DataDisplay.CubeAxesYAxisVisibility	=	YAxisVisibility
    DataDisplay.CubeAxesZAxisVisibility	=	ZAxisVisibility

    # Beschriften der Achsen
    DataDisplay.CubeAxesXTitle = XTitle
    DataDisplay.CubeAxesYTitle = YTitle
    DataDisplay.CubeAxesYTitle = ZTitle

    # Theoretisch
    #DataDisplay.CubeAxesXLabelFormat = '%-#6.0g'
    #DataDisplay.CubeAxesYLabelFormat = '%-#6.0g'
    #DataDisplay.CubeAxesZLabelFormat = '%-#6.0g'

    # Anzeigen der Ticks
    DataDisplay.CubeAxesXAxisTickVisibility	=	XAxisTickVisibility
    DataDisplay.CubeAxesYAxisTickVisibility	=	YAxisTickVisibility
    DataDisplay.CubeAxesZAxisTickVisibility	=	ZAxisTickVisibility

    # Anzeigen der "Zwischenticks"
    DataDisplay.CubeAxesXAxisMinorTickVisibility = XAxisMinorTickVisibility
    DataDisplay.CubeAxesYAxisMinorTickVisibility = YAxisMinorTickVisibility
    DataDisplay.CubeAxesZAxisMinorTickVisibility = ZAxisMinorTickVisibility

    # Eingegebenen Titel benutzen
    DataDisplay.CubeAxesUseDefaultXTitle	=	XTitleDisplay
    DataDisplay.CubeAxesUseDefaultYTitle	=	YTitleDisplay
    DataDisplay.CubeAxesUseDefaultZTitle	=	ZTitleDisplay

    # Hinterlegen der Achsen mit einem Gitter
    DataDisplay.CubeAxesGridLineLocation	=	 GridLineLocation

    # Rueckgabewerte
    return DataDisplay

# ===================================================================
# ===================================================================


def CreateLUT5Color(inputDataDisplay, Data, A=[], E=[]):

    # Anpassen des Datenbereichs der Look up table an den aktuellen
    # Wertebereich
    inputDataDisplay.RescaleTransferFunctionToDataRange(False)

    # Auslesen der Look up Table (LUT)
    LUT = pv.GetColorTransferFunction(Data)
    # Initialisieren des Wertebereiches fuer die LUT
    LUT.ScalarRangeInitialized= 1
    # Faerben im Bereich RGB - weisse Knoten vermeiden
    LUT.ColorSpace = 'RGB'
    #print(LUT.RGBPoints)

    if A == []:
        # Anpassen des Wertebereiches der LUT (Wert,R,G,B) hier fuer 5 Werte
        A = round(LUT.RGBPoints[0], 10)
        E = round(LUT.RGBPoints[8], 10)
        #print(E)
        #print(A)

        # Setzen der Grenzen des Wertebereiches so, dass diese voneinander
        # unterschiedlich sind.
        if A == E:
            A = LUT.RGBPoints[0] - 1
            E = LUT.RGBPoints[8] + 1
        #print(E)
        #print(A)

    # Berechnen von Stuetzstellen zwischen Minimal- und Maximalwert
    #print(A)
    B = A + (E-A)/4*1
    C = A + (E-A)/4*2
    D = A + (E-A)/4*3

    # Erzeugen einer neuen Look up Table mit 5 Stuetzwerten
    LUT2 = [A,0.231, 0.298, 0.753,\
            B,0,1,1,\
            C,0,1,0,\
            D,1,1,0,\
            E,0.706, 0.016, 0.150]

    # Uebernehmen der neuen Look up Table
    LUT.RGBPoints = LUT2
    #print(LUT.RGBPoints.GetData()) # Nur zur Ueberpruefung

    return LUT

# ===================================================================
# ===================================================================


def SetNachkommastellenLUT(LUT,Ausnahme=[], Data = ''):
    """
    Ausnahme : [[StringAusnahme1, StringFormat1],[StringAusnahme2, StringFormat2]]
    """
    # Einstellen der Anzahl der Nachkommastellen der Werte in Abhaengigkeit
    # von der Groesse der Werte.
    for Ausnahmex in Ausnahme:
        if Data == Ausnahmex[0]:
            labelFormat = Ausnahmex[1]
            #print(labelFormat)
            return labelFormat

    if abs(LUT.RGBPoints[16])<0.01:
        labelFormat="%-5.5f"
        #print(1)
    elif abs(LUT.RGBPoints[16])<1:
        labelFormat="%-5.3f"
        #print(2)
    elif abs(LUT.RGBPoints[16])<2:
        labelFormat="%-5.2f"
        #print(2)
    elif abs(LUT.RGBPoints[16])<25:
        labelFormat="%-5.1f"
        #print(2)
    else:
        labelFormat="%-5.0f"
        #print(4)
    #print(labelFormat)

    return labelFormat

# ===================================================================
# ===================================================================
