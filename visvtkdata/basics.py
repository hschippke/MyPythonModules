# -*- coding: utf-8 -*-
"""Module containing functions for basic operations.
"""

"""
Todo:
* ...
"""

"""
created  by: Henning Schippke
created  on: 06/2016
modified on: 03.02.2017
"""

import os

# ===================================================================
# ===================================================================


def GetFileNamesToVisualise(loadPath):
    """
    Loading files in folder *loadPath* ending in "\*.vtu", extracting data and
    appending it to the list *Data* and *Area* respectively.


    Parameters
    ----------
    loadPath : string
        String specifying path of input data folder containing vtu-files.


    Returns
    -------
    Area : list
        List of file names identical to "MeshMatLoad.vtu"

    Data : list
        List of vtu-file names in *loadPath*.


    Example
    -------
    Area, Data = GetFileNamesToVisualise(loadPath)
    """

    FilesInPath = os.listdir(loadPath)
    Area        = []
    Data        = []

    for EachInPath in FilesInPath:

        if EachInPath == 'MeshMatLoad.vtu':
            Area.append(EachInPath)

        elif EachInPath[-3:] == 'vtu':
            Data.append(EachInPath)

    return Area, Data

# ===================================================================
# ===================================================================


def TrimCreatedFigures(savePath, whiteBorder):
    """Trim created figures.

    Parameters
    ----------
    savePath : string
        path to folder, where created figures will be saved

    whiteBorder : floats
        value (in pixels) specifying white border margin


    Returns
    -------
    None


    Example
    -------
    TrimCreatedFigures(savePath, whiteBorder)
    """

    FilesInPath = [f for f in os.listdir(savePath) if not f.startswith('.')]

    os.chdir(savePath)

    for outFile in FilesInPath:
        filext = os.path.splitext(outFile)[1]

        if filext == '.png' or filext == '.jpg' :
            os.system("convert %s -trim -bordercolor white -border %d %s" %(outFile, whiteBorder, outFile))
