# -*- coding: utf-8 -*-
"""
Main module of package **visvtkdata** containig the essential functions

    * **ShowAreaInformation()**,
    * **ShowNodalInformation()** and
    * **ShowMeshQuality()**

creating the figures showing the data contained in the vtk-files, which have been
generated from the input data files to the FEM2Flow program.
"""

"""
Todo:
* ...
"""

"""
created  by Nicola Scholl
created  on 09/2015

modified by Henning Schippke
modified on 03.02.2017
"""


import paraview.simple as pv
import pvwrp
from myParaviewModules.basics import SetScalarBarSettings


# ===================================================================
# ===================================================================


def CreateAllFigures( loadPath, savePath,
                      Plotsettings,
                      Area, Data=[],
                      Qualityparam = [['Distortion'     ,'[-]'    ],
                                      ['Condition'      ,'[-]'    ],
                                      ['Scaled Jacobian','[-]'    ],
                                      ['Area'           ,'[m$^2$]']] ):
    """
    Visualise mesh, element and nodal data from vtk files as well as element quality.


    Parameters
    ----------
    loadPath : string
        Pfad unter dem die vtu-Dateien abgelegt sind.

    savePath: string
        Pfad unter dem die png-Bilder abgespeichert werden sollen.

    Plotsettings:
        Ein mit der Klasse CreatePlotSettings erzeugtes Objekt.
        Die Attribute sind die Einstellungen fuer die spaeteren
        Ausgabe.

    Area:
        Name der vtu-Datei (ohne Endung) in der sich die Flaechendaten bzw.
          Daten zur Geometrie befinden.

    Data:
        Name(n) der vtu-Datei(en) (ohne Endung) fuer die Knotendaten


    Returns
    -------
    None


    Example
    -------
    CreateAllFigures(loadPath, savePath, Plotsettings, Area, Data, Qualityparam)
    """

    # ---------------------------------------------------------------
    # Create mesh view as Background
    # ---------------------------------------------------------------

    renderView,\
    inputData,\
    inputDataDisplay = pvwrp.CreateBasicViewAndObject(loadPath, savePath,
                                    Area, Plotsettings)

    # ---------------------------------------------------------------
    # end | Create mesh view as Background
    # ---------------------------------------------------------------


    # ---------------------------------------------------------------
    # Create cell data figures
    # ---------------------------------------------------------------

    ShowAreaInformation(loadPath, savePath,
                        Area,
                        renderView,
                        inputData, inputDataDisplay, Plotsettings)

    # ---------------------------------------------------------------
    # end | Create cell data figures
    # ---------------------------------------------------------------


    # ---------------------------------------------------------------
    # Create nodal data figures
    # ---------------------------------------------------------------

    if Data != []:
        for Datax in Data:

            ShowNodalInformation(loadPath, savePath, Datax, renderView, Plotsettings)

    # ---------------------------------------------------------------
    # end | Create nodal data figures
    # ---------------------------------------------------------------


    # ---------------------------------------------------------------
    # Create mesh quality figures
    # ---------------------------------------------------------------

    if Qualityparam != []:
        for Qualityparamx in Qualityparam:

            units = Qualityparamx[1]

            ShowMeshQuality(Qualityparamx[0], units, inputData,
                            renderView, savePath, Plotsettings)

    # ---------------------------------------------------------------
    # end | Create mesh quality figures
    # ---------------------------------------------------------------


# ===================================================================
# ===================================================================



# ===================================================================
# Create cell data representations
# ===================================================================

def ShowAreaInformation(loadPath, savePath, Area, renderView, inputData, \
                        inputDataDisplay,Plotsettings):
    """
    Create elementDataDistribution (elementID, matID, loadID) and spatialDiscretisation
    (mesh with nodes and surface normal direction value) figures.


    InputData
    ---------
    loadPath
        Pfad unter dem die vtu-Dateien abgelegt sind.

    savePath
        Pfad unter dem die png-Bilder abgespeichert werden sollen.

    Area:
        Name der vtu-Datei (ohne Endung) in der sich die Flaechendaten bzw.
        Daten zur Geometrie befinden.

    renderView
        Enthaelt Einstellungen zu der Kamera und zu der Pixel-Groesse des Bildes.

    inputData
        Enthaelt Daten ueber die Geometrie (Paraview).

    inputDataDisplay
        Enthaelt Daten zu der Darstellung der Geometrie (Paraview).

    Plotsettings
        Ein mit der Klasse CreatePlotSettings erzeugtes Objekt.
        Die Attribute sind die Einstellungen fuer die spaeteren Ausgabe.


    OutputData
    ----------
    None


    Usage
    -----
    ShowAreaInformation(loadPath, savePath, Area, renderView, inputData, inputDataDisplay,Plotsettings)
    """


    # Liste, die alle Flaechendaten, die geplottet werden sollen, enthaelt ----
    PlotData = inputData.CellArrayStatus[:] # Liste aus Paraview
    PlotData.append('nodeNumber')
    PlotData.append('cellNormals')


    for Datax in PlotData:

        # Einfaerben der Teilflaechen unter Beruecksichtigung der Magnitude
        # des jeweiligen Datensatzes Datax -----------------------------------
        if Datax == 'nodeNumber':
            pv.ColorBy(inputDataDisplay, ('Points', Datax))
        else:
            pv.ColorBy(inputDataDisplay, ('CELLS', Datax))

        # Erstellen der Look up Table -----------------------------------------
        # Anpassen des Wertebereiches fuer cellNormals
        if Datax == 'cellNormals':
            A = -2.0
            E =  2.0

            # Erstellen der Look up Table
            LUT = pvwrp.CreateLUT5Color(inputDataDisplay, Datax,A,E)
        else:
            # Erstellen der Look up Table
            LUT = pvwrp.CreateLUT5Color(inputDataDisplay, Datax)

        # Erstellen der Colorbar bzw. ScalarBar -------------------------------
        scalarBar = pv.CreateScalarBar()

        # Einstellen der Anzahl der Nachkommastellen der Werte in Abhaengigkeit
        # von der Groesse der Werte.
        labelFormat = pvwrp.SetNachkommastellenLUT(LUT,[['elementTypeID',"%-5.1f"]], Datax)
        #print(labelFormat)

        # Anpassen der ScalarBar in Abhaengigkeit von dem Datensatz
        if Datax == 'cellNormals':
            SetScalarBarSettings(scalarBar,
                                 lookupTable      = LUT,
                                 title            = 'z-direction surface normal',
                                 title2           = '[-]',
                                 orientation      = Plotsettings.scalarBarOrientation,
                                 position         = Plotsettings.scalarBarPosition,
                                 position2        = Plotsettings.scalarBarPosition2,
                                 titleFontFamily  = 'Arial',
                                 titleFontSize    = Plotsettings.scalarBarTitleFontSize,
                                 labelFontFamily  = 'Arial',
                                 labelFontSize    = Plotsettings.scalarBarLabelFontSize,
                                 labelFormat      = labelFormat,
                                 labelRangeFormat = labelFormat,
                                 labelNumberOf    = 9)

        else:
            SetScalarBarSettings(scalarBar,
                                 lookupTable      = LUT,
                                 title            = Datax,
                                 title2           = '[-]',
                                 orientation      = Plotsettings.scalarBarOrientation,
                                 position         = Plotsettings.scalarBarPosition,
                                 position2        = Plotsettings.scalarBarPosition2,
                                 titleFontFamily  = 'Arial',
                                 titleFontSize    = Plotsettings.scalarBarTitleFontSize,
                                 labelFontFamily  = 'Arial',
                                 labelFontSize    = Plotsettings.scalarBarLabelFontSize,
                                 labelFormat      = labelFormat,
                                 labelRangeFormat = labelFormat,
                                 labelNumberOf    = 5)


        # Darstellen der Scalarbar
        renderView.Representations.append(scalarBar)

        # Erzeugen des png-Plots mit pv.SaveScreenshot ------------------------
        if Datax == 'cellNormals':
            # Darstellen der Knoten (Schwarz)
            (renderView, inputDataNode, inputDataDisplayNode) = ShowNodes(loadPath, savePath, Area, renderView, Plotsettings)
            DataPlot = 'spatialDiscretisation_surfaceNormal'
        else:
            DataPlot = 'elementDataDistribution_' + Datax

        print('Plotte: ' + DataPlot)
        pv.SaveScreenshot(savePath + DataPlot.replace(" ","") + '.' + Plotsettings.outputFileType, magnification = Plotsettings.magnification, quality=100, view=renderView)
        print(DataPlot + ' geplottet \n')

        # Zuruecksetzen einiger Parameter fuer den naechsten Plot -------------
        # Ausstellen der "alten" Scalarbar
        renderView.Representations.remove(scalarBar)

        if Datax == 'cellNormals':
            # Entfernen der Farben der Elemente
            pv.ColorBy(inputDataDisplay, 'None')

            DataPlot = 'spatialDiscretisation_mesh'
            print('Plotte: ' + DataPlot)
            pv.SaveScreenshot(savePath + DataPlot.replace(" ","") + '.' + Plotsettings.outputFileType, magnification = Plotsettings.magnification, quality=100, view=renderView)
            print(DataPlot + ' geplottet \n')

    # Ausstellen der Sichbarkeit der schwarzen Knoten
    inputDataDisplayNode.Visibility = 0

    # Entfernen der Farben der Elemente
    pv.ColorBy(inputDataDisplay, 'None')

# ===================================================================
# end | Create cell data representations
# ===================================================================


# ===================================================================
# Show point positions
# ===================================================================

def ShowNodes(loadPath, savePath, Area, renderView, Plotsettings):
    """
    Plot node positions on top of mesh representation (area information).

    Parameters
    ----------
    loadPath : string
        Pfad unter dem die vtu-Dateien abgelegt sind.

    renderView
        Enthaelt Einstellungen zu der Kamera und zu der Pixel-Groesse des Bildes.

    Plotsettings
        Ein mit der Klasse CreatePlotSettings erzeugtes Objekt.
        Die Attribute sind die Einstellungen fuer die spaeteren Ausgabe.

    OutputData
    ----------
    renderView
        Enthaelt Einstellungen zu der Kamera und zu der Pixel-Groesse des Bildes.

    inputData
        Enthaelt Daten ueber die Geometrie (Paraview).

    inputDataDisplay
        Enthaelt Daten zu der Darstellung der Geometrie (Paraview).

    Example
    -------
    renderView, inputdata, inputDataDisplay = ShowNodes(loadPath, savePath, Area, renderView, Plotsettings)
    """


    # Einlesen und Interpretieren der vtu-Datei -------------------------------
    inputData, filename = pvwrp.ReadData(loadPath, Area)
#    inputData = pv.XMLUnstructuredGridReader(FileName=[loadPath])

    # Erzeugen der Darstellung der Geometrie in Paraview ----------------------
    inputDataDisplay = pv.GetDisplayProperties(inputData, view=renderView)

    # Darstellung der Knoten im zweiten "Layer" -------------------------------
    inputDataDisplay.SetRepresentationType('Points')

    # Aendern der "Hintergrundfarbe" der Geometrie (Schwarz) ------------------
    inputDataDisplay.AmbientColor = [0.0, 0.0, 0.0]

    # Groesse der Punkte in Pixeln --------------------------------------------
    inputDataDisplay.PointSize = Plotsettings.pointSize

    # Entfernen der Farbe der Knoten/Punkte, Hintergrundfarbe wird zu Knotenfarbe
    pv.ColorBy(inputDataDisplay, None)

    # Rueckgabewerte
    return (renderView, inputData, inputDataDisplay)

# ===================================================================
# end | Show point positions
# ===================================================================


# ===================================================================
# Create node data representations
# ===================================================================

def ShowNodalInformation(loadPath, savePath, Data, renderView, Plotsettings):
    """
    Plot all initial (condInitial_xx) and boundary value (condBoundDir_xx) representation figures.

    Parameters
    ----------
    loadPath : string
        Pfad unter dem die vtu-Dateien abgelegt sind.

    savePath : string
        Pfad unter dem die png-Bilder abgespeichert werden sollen.

    Data
        Name(n) der vtu-Datei(en) (ohne Endung) fuer die Knotendaten

    renderView
        Enthaelt Einstellungen zu der Kamera und zu der Pixel-Groesse des Bildes.

    Plotsettings
        Ein mit der Klasse CreatePlotSettings erzeugtes Objekt.
        Die Attribute sind die Einstellungen fuer die spaeteren Ausgabe.


    Returns
    -------
    None


    Example
    -------
    ShowNodalInformation(loadPath, savePath, Data, renderView, Plotsettings)
    """


    # Einlesen und Interpretieren der jeweiligen vtu-Datei --------------------
    inputData, filename = pvwrp.ReadData(loadPath, Data)
    print(filename)
    #inputData = pv.XMLUnstructuredGridReader(FileName=[loadPath + Data])

    # Erzeugen der Darstellung der Geometrie in Paraview ----------------------
    inputDataDisplay = pv.GetDisplayProperties(inputData, view=renderView)

    # Einfaerben der Knoten unter Beruecksichtigung der Magnitude
    # des jeweiligen Datensatzes Data -----------------------------------------
    pv.ColorBy(inputDataDisplay, ('Points', filename))

    # Erstellen der Look up Table ---------------------------------------------
    LUT = pvwrp.CreateLUT5Color(inputDataDisplay, filename)

    # Erstellen der Colorbar bzw. ScalarBar -----------------------------------
    scalarBar = pv.CreateScalarBar()

    # Einstellen der Anzahl der Nachkommastellen der Werte in Abhaengigkeit
    # von der Groesse der Werte.
    labelFormat = pvwrp.SetNachkommastellenLUT(LUT)

    # Anpassen der ScalarBar in Abhaengigkeit von dem Datensatz
    SetScalarBarSettings(scalarBar,
                         lookupTable      = LUT,
                         title            = 'value',
                         title2           = '[dofID dependent]',
                         orientation      = Plotsettings.scalarBarOrientation,
                         position         = Plotsettings.scalarBarPosition,
                         position2        = Plotsettings.scalarBarPosition2,
                         titleFontFamily  = 'Arial',
                         titleFontSize    = Plotsettings.scalarBarTitleFontSize,
                         labelFontFamily  = 'Arial',
                         labelFontSize    = Plotsettings.scalarBarLabelFontSize,
                         labelFormat      = labelFormat,
                         labelRangeFormat = labelFormat,
                         labelNumberOf    = 5)

    # Groesse der Punkte in Pixeln --------------------------------------------
    inputDataDisplay.PointSize = Plotsettings.pointSize

    # Darstellen der Scalarbar
    renderView.Representations.append(scalarBar)

    # Erzeugen des png-Plots mit pv.SaveScreenshot ------------------------
    print('Plotte: ' + filename)
    pv.SaveScreenshot(savePath + filename.replace(" ","") + '.' + Plotsettings.outputFileType, magnification = Plotsettings.magnification, quality=100, view=renderView)
    print(filename + ' geplottet \n')

    # Zuruecksetzen einiger Parameter fuer den naechsten Plot -------------
    # Ausstellen des "alten" Datensatzes
    inputDataDisplay.Visibility = 0

    # Ausstellen der "alten" Scalarbar
    renderView.Representations.remove(scalarBar)

# ===================================================================
# end | Create node data representations
# ===================================================================


# ===================================================================
# Create mesh quality representations
# ===================================================================

def ShowMeshQuality(meshQualityMeasure, units, sourceData, renderView, savePath, Plotsettings):
    """ Create mesh quality plots.

    Parameters
    ----------
    meshQualityMeasure : list of strings
        Parameter zur Berechnung der Qualitaet (z.B. 'Area','Scaled Jacobian', 'Condition', 'Distortion')

    units
        Zu den Parametern gehoerende Einheiten.

    sourceData
        Datensatz aus dem die Qualitaetsmerkmale berechnet werden.

    renderVie
        Enthaelt Einstellungen zu der Kamera und zu der Pixel-Groesse des Bildes.

    savePath : string
        Pfad unter dem die png-Bilder abgespeichert werden sollen.

    Plotsettings
        Ein mit der Klasse CreatePlotSettings erzeugtes Objekt.
        Die Attribute sind die Einstellungen fuer die spaeteren Ausgabe.

    Returns
    -------
    None


    Exmaple
    -------
    ShowMeshQuality(meshQualityMeasure, units, sourceData, renderView, savePath, Plotsettings)
    """


    # Berechnen der Mesh Quailty fuer den Parameter in meshQualityMeasure
    meshQuality1                    = pv.MeshQuality(Input=sourceData)
    meshQuality1.QuadQualityMeasure = meshQualityMeasure
    meshQuality1Display             = pv.Show(meshQuality1, renderView)

    # Einfaerben Elemente
    pv.ColorBy(meshQuality1Display, ('CELLS', 'Quality'))

    # Darstellung Elementgrenzen
    meshQuality1Display.SetRepresentationType('Surface With Edges')
    meshQuality1Display.EdgeColor = [0.0, 0.0, 0.0] # (schwarz)

    # Erstellen der Look up Table
    LUT = pvwrp.CreateLUT5Color(meshQuality1Display, 'Quality')

    # Erstellen der Colorbar bzw. ScalarBar -----------------------------------
    scalarBar = pv.CreateScalarBar()

    # Einstellen der Anzahl der Nachkommastellen der Werte in Abhaengigkeit
    # von der Groesse der Werte.
    labelFormat = pvwrp.SetNachkommastellenLUT(LUT)

    # Anpassen der ScalarBar in Abhaengigkeit von dem Datensatz
    SetScalarBarSettings(scalarBar,
                         lookupTable      = LUT,
                         title            = meshQualityMeasure,
                         title2           = units,
                         orientation      = Plotsettings.scalarBarOrientation,
                         position         = Plotsettings.scalarBarPosition,
                         position2        = Plotsettings.scalarBarPosition2,
                         titleFontFamily  = 'Arial',
                         titleFontSize    = Plotsettings.scalarBarTitleFontSize,
                         labelFontFamily  = 'Arial',
                         labelFontSize    = Plotsettings.scalarBarLabelFontSize,
                         labelFormat      = labelFormat,
                         labelRangeFormat = labelFormat,
                         labelNumberOf    = 5)

    # Darstellen der Scalarbar
    renderView.Representations.append(scalarBar)

    # ---------------------------------------------------------------
    # Plotten
    # ---------------------------------------------------------------

    # Erzeugen des Speichernamens und des Speicherpfades
    saveName = 'MeshQuality-'+ meshQualityMeasure
    saveFile = savePath + saveName.replace(" ", "") + '.' + Plotsettings.outputFileType

    # Erzeugen Plots mit pv.SaveScreenshot
    print('Plotte: ' + saveName)
    pv.SaveScreenshot(saveFile, magnification = Plotsettings.magnification, quality=100, view=renderView)
    print(saveName + ' geplottet \n')

    # ---------------------------------------------------------------
    # end | Plotten
    # ---------------------------------------------------------------

    # Zuruecksetzen einiger Parameter fuer naechsten Plot
    # Ausstellen des "alten" Datensatzes
    meshQuality1Display.Visibility = 0

    # Ausstellen der "alten" Scalarbar
    renderView.Representations.remove(scalarBar)


# ===================================================================
# end | Create mesh quality representations
# ===================================================================

# ===================================================================
# ===================================================================
