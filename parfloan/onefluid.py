# -*- coding: utf-8 -*-

"""
Module containing functions needed for performing flow analysis of
single fluid flows using the python api of Paraview.
"""

"""
created  by Henning Schippke
created  on 01.03.17
modified on 21.03.17
"""

import numpy                                 as np
import paraview.simple                       as pv
from   myParaviewModules import basics       as paraViewBasics
from   basics            import DataAnalysis

# ===================================================================
# ===================================================================


class SingleFluidFlowAnalysis(DataAnalysis):
    """Flow analysis of single fluid flows.

    Methods
    -------


    Attributes
    ----------


    Example
    -------


    """

    def __init__(self, loadPath, loadName,
                       savePath, saveName,
                       timeInstant,
                       **kwargs):

        super(SingleFluidFlowAnalysis, self).__init__(loadPath, loadName,
                                                      savePath, saveName,
                                                      timeInstant,
                                                      **kwargs)

        self.scBrTitleFontSize = self.fontSize
        self.scBrLabelFontSize = self.fontSize - 1

    # ===============================================================
    # ===============================================================

    # ===============================================================
    # Methods
    # ===============================================================

    def ShowVelocityField(self, subHeadingPosition = [0.45, 0.85], **kwargs):

        self.InitializeViewAndLoadData(**kwargs)

        self.VelocityField(**kwargs)

        subheading = { 'text'     : "Geschwindigkeitsfeld",
                       'position' : subHeadingPosition}
        self.CreateTitle(subheadingDict = subheading, **kwargs)

        self.CreateTimeAnnotation(**kwargs)

        self.CreateCalcExampleDescription(**kwargs)

        self.saveNameFigure = self.saveName + '_Velocity'
        self.SaveFigure()

    # ===============================================================


    def ShowVelocityWithVectors(self, subHeadingPosition = [0.4105, 0.85], **kwargs):

        self.InitializeViewAndLoadData(**kwargs)

        self.VelocityField(**kwargs)

        self.VelocityVectors(**kwargs)

        subheading = { 'text'     : "Geschwindigkeitsfeld mit -vektoren",
                       'position' : subHeadingPosition}
        self.CreateTitle(subheadingDict = subheading, **kwargs)

        self.CreateTimeAnnotation(**kwargs)

        self.CreateCalcExampleDescription(**kwargs)

        self.saveNameFigure = self.saveName + '_VelVec'
        self.SaveFigure()

    # ===============================================================


    def ShowVelocityWithStreamlines(self, subHeadingPosition = [0.41, 0.85], **kwargs):

        self.InitializeViewAndLoadData(**kwargs)

        self.VelocityField(**kwargs)

        self.Streamlines(**kwargs)

        subheading = { 'text'     : "Geschwindigkeitsfeld mit Stromlinien",
                       'position' : subHeadingPosition}
        self.CreateTitle(subheadingDict = subheading, **kwargs)

        self.CreateTimeAnnotation(**kwargs)

        self.CreateCalcExampleDescription(**kwargs)

        self.saveNameFigure = self.saveName + '_VelStream'
        self.SaveFigure()

    # ===============================================================


    def ShowVelocityWithVectorsAndStreamlines(self, subHeadingPosition = [0.37, 0.85], **kwargs):

        self.InitializeViewAndLoadData(**kwargs)

        self.VelocityField(**kwargs)

        self.Streamlines(**kwargs)

        self.VelocityVectors(**kwargs)

        subheading = { 'text'     : "Geschwindigkeitsfeld mit -vektoren und Stromlinien",
                       'position' : subHeadingPosition}
        self.CreateTitle(subheadingDict = subheading, **kwargs)

        self.CreateTimeAnnotation(**kwargs)

        self.CreateCalcExampleDescription(**kwargs)

        self.saveNameFigure = self.saveName + '_VelVecStream'
        self.SaveFigure()

    # ===============================================================




    def ShowVelocityGradient(self, subHeadingPosition = [0.425, 0.85], **kwargs):

        self.InitializeViewAndLoadData(**kwargs)

        self.VelocityGradient(**kwargs)

        subheading = { 'text'     : "Geschwindigkeitsfeld, Gradient",
                       'position' : subHeadingPosition}
        self.CreateTitle(subheadingDict = subheading, **kwargs)

        self.CreateTimeAnnotation(**kwargs)

        self.CreateCalcExampleDescription(**kwargs)

        self.saveNameFigure = self.saveName + '_VelGrad'
        self.SaveFigure()

    # ===============================================================


    def ShowVelocityDivergence(self, subHeadingPosition = [0.425, 0.85], **kwargs):

        self.InitializeViewAndLoadData(**kwargs)

        self.VelocityDivergence(**kwargs)

        self.CalcDivergence(**kwargs)

        subheading = { 'text'     : "Geschwindigkeitsfeld, Divergenz",
                       'position' : subHeadingPosition}
        self.CreateTitle(subheadingDict = subheading, **kwargs)

        self.CreateTimeAnnotation(**kwargs)

        self.CreateCalcExampleDescription(**kwargs)

        self.saveNameFigure = self.saveName + '_VelDiv'
        self.SaveFigure()

    # ===============================================================


    def ShowVelocityDivergenceWithIsosurfaces(self, subHeadingPosition = [0.40, 0.85], **kwargs):

        self.InitializeViewAndLoadData(**kwargs)

        self.VelocityDivergence(**kwargs)

        self.CalcDivergence(**kwargs)

        self.DivergenceIsosurfaces(**kwargs)

        subheading = { 'text'     : "Geschwindigkeitsfeld, Divergenz; Isolinien",
                       'position' : subHeadingPosition}
        self.CreateTitle(subheadingDict = subheading, **kwargs)

        self.CreateTimeAnnotation(**kwargs)

        self.CreateCalcExampleDescription(**kwargs)

        self.saveNameFigure = self.saveName + '_VelDivIso'
        self.SaveFigure()

    # ===============================================================


    def ShowVorticityField(self, subHeadingPosition = [0.389, 0.85], **kwargs):

        self.InitializeViewAndLoadData(**kwargs)

        self.VorticityField(**kwargs)

        subheading = { 'text'     : "Wirbelstärke (Geschwindigkeitsfeld, Rotation)",
                       'position' : subHeadingPosition}
        self.CreateTitle(subheadingDict = subheading, **kwargs)

        self.CreateTimeAnnotation(**kwargs)

        self.CreateCalcExampleDescription(**kwargs)

        self.saveNameFigure = self.saveName + '_VelRot'
        self.SaveFigure()

    # ===============================================================




    def ShowMomentumField(self, subHeadingPosition = [0.47, 0.85], **kwargs):

        self.InitializeViewAndLoadData(**kwargs)

        self.MomentumField(**kwargs)

        subheading = { 'text'     : "Impulsfeld",
                       'position' : subHeadingPosition}
        self.CreateTitle(subheadingDict = subheading, **kwargs)

        self.CreateTimeAnnotation(**kwargs)

        self.CreateCalcExampleDescription(**kwargs)

        self.saveNameFigure = self.saveName + '_Momentum'
        self.SaveFigure()

    # ===============================================================


    def ShowEnergyField(self, subHeadingPosition = [0.47, 0.85], **kwargs):

        self.InitializeViewAndLoadData(**kwargs)

        self.EnergyField(**kwargs)

        subheading = { 'text'     : "Energiefeld",
                       'position' : subHeadingPosition}
        self.CreateTitle(subheadingDict = subheading, **kwargs)

        self.CreateTimeAnnotation(**kwargs)

        self.CreateCalcExampleDescription(**kwargs)

        self.saveNameFigure = self.saveName + '_Energy'
        self.SaveFigure()

    # ===============================================================


    def ShowEnergyWithIsosurfaces(self, subHeadingPosition = [0.44, 0.85], **kwargs):

        self.InitializeViewAndLoadData(**kwargs)

        self.EnergyField(**kwargs)

        self.EnergyIsosurfaces(**kwargs)

        subheading = { 'text'     : "Energiefeld mit Isolinien",
                       'position' : subHeadingPosition}
        self.CreateTitle(subheadingDict = subheading, **kwargs)

        self.CreateTimeAnnotation(**kwargs)

        self.CreateCalcExampleDescription(**kwargs)

        self.saveNameFigure = self.saveName + '_EnergyIso'
        self.SaveFigure()

    # ===============================================================




    def ShowPressureField(self, subHeadingPosition = [0.48, 0.85], **kwargs):

        self.InitializeViewAndLoadData(**kwargs)

        self.PressureField(**kwargs)

        subheading = { 'text'     : "Druckfeld",\
                       'position' : subHeadingPosition }
        self.CreateTitle(subheadingDict = subheading, **kwargs)

        self.CreateTimeAnnotation(**kwargs)

        self.CreateCalcExampleDescription(**kwargs)

        self.saveNameFigure = self.saveName + '_Pressure'
        self.SaveFigure()

    # ===============================================================


    def ShowPressureWithIsobars(self, subHeadingPosition = [0.44, 0.85], **kwargs):

        self.InitializeViewAndLoadData(**kwargs)

        self.PressureField(**kwargs)

        self.PressureIsolines(**kwargs)

        subheading = { 'text'     : "Druckfeld mit Isobaren",\
                       'position' : subHeadingPosition }
        self.CreateTitle(subheadingDict = subheading, **kwargs)

        self.CreateTimeAnnotation(**kwargs)

        self.CreateCalcExampleDescription(**kwargs)

        self.saveNameFigure = self.saveName + '_PressureIso'
        self.SaveFigure()

    # ===============================================================


    def ShowPressureWithVelocityVectors(self, subHeadingPosition = [0.405, 0.85], **kwargs):

        self.InitializeViewAndLoadData(**kwargs)

        self.PressureField(**kwargs)

        self.VelocityVectors(velVecColor = [1.0, 1.0, 1.0], **kwargs)

        subheading = { 'text'     : "Druckfeld mit Geschwindigkeitsvektoren",\
                       'position' : subHeadingPosition }
        self.CreateTitle(subheadingDict = subheading, **kwargs)

        self.CreateTimeAnnotation(**kwargs)

        self.CreateCalcExampleDescription(**kwargs)

        self.saveNameFigure = self.saveName + '_PressureVelVec'
        self.SaveFigure()

    # ===============================================================


    def ShowPressureWithIsobarsAndVelocityVectors(self, subHeadingPosition = [0.37, 0.85], **kwargs):

        self.InitializeViewAndLoadData(**kwargs)

        self.PressureField(**kwargs)

        self.PressureIsolines(isobarOpacity = 0.4, **kwargs)

        self.VelocityVectors(velVecColor = [1.0, 1.0, 1.0], **kwargs)

        subheading = { 'text'     : "Druckfeld mit Isobaren und Geschwindigkeitsvektoren",\
                       'position' : subHeadingPosition }
        self.CreateTitle(subheadingDict = subheading, **kwargs)

        self.CreateTimeAnnotation(**kwargs)

        self.CreateCalcExampleDescription(**kwargs)

        self.saveNameFigure = self.saveName + '_PressureIsoVelVec'
        self.SaveFigure()

    # ===============================================================


    def ShowPressureWithStreamlines(self, subHeadingPosition = [0.44, 0.85], **kwargs):

        self.InitializeViewAndLoadData(**kwargs)

        self.PressureField(**kwargs)

        self.Streamlines(**kwargs)

        subheading = { 'text'     : "Druckfeld mit Stromlinien",\
                       'position' : subHeadingPosition }
        self.CreateTitle(subheadingDict = subheading, **kwargs)

        self.CreateTimeAnnotation(**kwargs)

        self.CreateCalcExampleDescription(**kwargs)

        self.saveNameFigure = self.saveName + '_PressureStream'
        self.SaveFigure()

    # ===============================================================


    def ShowPressureWithVelocityVectorsAndStreamlines(self, subHeadingPosition = [0.365, 0.85], **kwargs):

        self.InitializeViewAndLoadData(**kwargs)

        self.PressureField(**kwargs)

        self.Streamlines(**kwargs)

        self.VelocityVectors(**kwargs)

        subheading = { 'text'     : "Druckfeld mit Geschwindigkeitsvektoren und Stromlinien",\
                       'position' : subHeadingPosition }
        self.CreateTitle(subheadingDict = subheading, **kwargs)

        self.CreateTimeAnnotation(**kwargs)

        self.CreateCalcExampleDescription(**kwargs)

        self.saveNameFigure = self.saveName + '_PressureVelVecStream'
        self.SaveFigure()

    # ===============================================================


    # ===============================================================
    # ===============================================================


    def VelocityField(self,
                      scBrTitle              = 'velocity mag.',
                      scBrTitle2             = '[m/s]',
                      scBrTitleJustification = 'Centered',
                      scBrPosition           = None,
                      scBrPosition2          = None,
                      scBrOrientation        = None,
                      scBrLabelNumber        = 3,
                      scBrLabelFormat        = '% 5.2f',
                      **kwargs):

        # # show data
        # self.dataDisp = pv.Show(self.data, self.renderView)

        # show velocity data
        pv.ColorBy(self.dataDisp, ('Points', 'velocity'))
        self.dataDisp.RescaleTransferFunctionToDataRange(True)
        self.dataDisp.SetScalarBarVisibility(self.renderView, True)

        # show and adjust color bar
        velocityLUT       = pv.GetColorTransferFunction('velocity')
        self.velocityScBr = pv.GetScalarBar(velocityLUT,self.renderView)

        scBrTitleFontFamily    = self.fontFamily
        scBrTitleFontSize      = self.scBrTitleFontSize

        scBrLabelRangeFormat   = scBrLabelFormat

        scBrLabelFontFamily    = self.fontFamily
        scBrLabelFontSize      = self.scBrLabelFontSize

        paraViewBasics.SetScalarBarSettings(self.velocityScBr                   ,
                                    title              = scBrTitle              ,
                                    title2             = scBrTitle2             ,
                                    titleFontFamily    = scBrTitleFontFamily    ,
                                    titleFontSize      = scBrTitleFontSize      ,
                                    titleJustification = scBrTitleJustification ,
                                    position           = scBrPosition           ,
                                    position2          = scBrPosition2          ,
                                    orientation        = scBrOrientation        ,
                                    labelNumberOf      = scBrLabelNumber        ,
                                    labelFormat        = scBrLabelFormat        ,
                                    labelRangeFormat   = scBrLabelRangeFormat   ,
                                    labelFontFamily    = scBrLabelFontFamily    ,
                                    labelFontSize      = scBrLabelFontSize       )

    # ===============================================================


    def VelocityVectors(self,
                        velVecNumbSamplePoints = 1000,
                        velVecScaleFac         = 0.5,
                        velVecGlyphType        = '2D Glyph',
                        velVecColor            = None,
                        velVecOpacity          = None,
                        **kwargs):

        if velVecGlyphType == 'Arrow':
            if velVecColor is None:
                velVecColor = 'GlyphVector'
            if velVecOpacity is None:
                velVecOpacity   = 1.0

        elif velVecGlyphType == '2D Glyph':
            if velVecColor is None:
                velVecColor = [1.0, 1.0, 1.0]
            if velVecOpacity is None:
                velVecOpacity   = 1.0

        else:
            raise ValueError('glyph type not yet implemented.')

        # create velocity vectors
        velVec                             = pv.Glyph(Input=self.data, GlyphType='Arrow')
        velVec.Vectors                     = ['POINTS', 'velocity']
        velVec.ScaleMode                   = 'vector'
        velVec.ScaleFactor                 = velVecScaleFac
        velVec.MaximumNumberOfSamplePoints = velVecNumbSamplePoints
        velVec.GlyphType                   = velVecGlyphType
        if velVecGlyphType is '2D Glyph':
            velVec.GlyphType.GlyphType = 'HookedArrow'


        # show data in view
        velVecDisp = pv.Show(velVec, self.renderView)
        # pv.ColorBy(velVecDisp, ('POINTS', 'velocity'))
        if isinstance(velVecColor, basestring):
            pv.ColorBy(velVecDisp, ('POINTS', velVecColor))
            velVecDisp.RescaleTransferFunctionToDataRange(True)
            velVecDisp.SetScalarBarVisibility(self.renderView, False)
            velVecDisp.Opacity      = velVecOpacity
        elif isinstance(velVecColor, list):
            pv.ColorBy(velVecDisp, None)
            velVecDisp.DiffuseColor = velVecColor
            velVecDisp.Opacity      = velVecOpacity
        else:
            raise TypeError('Color specification of velocity vectors must be either a string or a list of ints [r,g,b].')

        pv.SetActiveSource(self.data)

    # ===============================================================


    def Streamlines(self, streamlineLengthMax      = 15.0,
                          streamlineSeedPoint1     = None,
                          streamlineSeedPoint2     = None,
                          streamlineSeedResolution = 20,
                          streamlineColor          = [0.0, 0.0, 0.0],
                          streamlineOpacity        = 0.3,
                          **kwargs):

        # create streamline plot
        self.streamlines = pv.StreamTracer(Input=self.data, SeedType='High Resolution Line Source')

        self.streamlines.SeedType.Resolution = streamlineSeedResolution
        if streamlineSeedPoint1 is not None:
            self.streamlines.SeedType.Point1 = streamlineSeedPoint1
        if streamlineSeedPoint2 is not None:
            self.streamlines.SeedType.Point2 = streamlineSeedPoint2

        self.streamlines.Vectors                 = ['POINTS', 'velocity']
        self.streamlines.MaximumStreamlineLength = streamlineLengthMax

        self.streamlinesDisp              = pv.Show(self.streamlines, self.renderView)
        self.streamlinesDisp.LineWidth    = self.lineWidth
        self.streamlinesDisp.DiffuseColor = streamlineColor
        self.streamlinesDisp.Opacity      = streamlineOpacity

        pv.ColorBy(self.streamlinesDisp, None )
        # pv.ColorBy(self.streamlinesDisp, ('Points','Vorticity') )
        # self.streamlinesDisp.RescaleTransferFunctionToDataRange(True)
        # self.streamlinesDisp.SetScalarBarVisibility(self.renderView, False)
        # vorticityLUT = pv.GetColorTransferFunction('Vorticity')
        # vorticityPWF = pv.GetOpacityTransferFunction('Vorticity')

        pv.SetActiveSource(self.data)

    # ===============================================================




    def CalcGradient(self, dataField, computeDivergence = 1, computeVorticity = 1):

        self.dataGrad = pv.GradientOfUnstructuredDataSet(Input=self.data)

        self.dataGrad.ScalarArray         = ['POINTS', dataField]
        self.dataGrad.ResultArrayName     = dataField + 'Gradient'

        self.dataGrad.ComputeDivergence   = computeDivergence
        if computeDivergence:
            self.dataGrad.DivergenceArrayName = dataField + 'Divergence'

        self.dataGrad.ComputeVorticity    = computeVorticity
        if computeVorticity:
            self.dataGrad.VorticityArrayName  = dataField + 'Rotation'

        self.dataGradDisp = pv.Show(self.dataGrad, self.renderView)

    # ===============================================================


    def VelocityGradient(self,
                         scBrTitle              = 'velocity gradient mag.',
                         scBrTitle2             = '[1/s]',
                         scBrTitleJustification = 'Centered',
                         scBrPosition           = None,
                         scBrPosition2          = None,
                         scBrOrientation        = None,
                         scBrLabelNumber        = 3,
                         scBrLabelFormat        = '% 5.2f',
                         **kwargs):

        self.CalcGradient('velocity')

        # show velocity gradient
        pv.ColorBy(self.dataGradDisp, ('Points', 'velocityGradient'))

        self.dataGradDisp.RescaleTransferFunctionToDataRange(True)
        self.dataGradDisp.SetScalarBarVisibility(self.renderView, True)

        # show and adjust color bar
        velocityGradLUT  = pv.GetColorTransferFunction('velocityGradient')
        velocityGradScBr = pv.GetScalarBar(velocityGradLUT,self.renderView)

        scBrTitleFontFamily    = self.fontFamily
        scBrTitleFontSize      = self.scBrTitleFontSize

        scBrLabelRangeFormat   = scBrLabelFormat

        scBrLabelFontFamily    = self.fontFamily
        scBrLabelFontSize      = self.scBrLabelFontSize

        paraViewBasics.SetScalarBarSettings(velocityGradScBr                   ,
                                    title              = scBrTitle              ,
                                    title2             = scBrTitle2             ,
                                    titleFontFamily    = scBrTitleFontFamily    ,
                                    titleFontSize      = scBrTitleFontSize      ,
                                    titleJustification = scBrTitleJustification ,
                                    position           = scBrPosition           ,
                                    position2          = scBrPosition2          ,
                                    orientation        = scBrOrientation        ,
                                    labelNumberOf      = scBrLabelNumber        ,
                                    labelFormat        = scBrLabelFormat        ,
                                    labelRangeFormat   = scBrLabelRangeFormat   ,
                                    labelFontFamily    = scBrLabelFontFamily    ,
                                    labelFontSize      = scBrLabelFontSize       )


    # ===============================================================


    def VelocityDivergence(self,
                           scBrTitle              = 'velocity divergence',
                           scBrTitle2             = '[1/s]',
                           scBrTitleJustification = 'Centered',
                           scBrPosition           = None,
                           scBrPosition2          = None,
                           scBrOrientation        = None,
                           scBrLabelNumber        = 3,
                           scBrLabelFormat        = '% 5.2f',
                           **kwargs):

        self.CalcGradient('velocity')

        # show velocity gradient
        pv.ColorBy(self.dataGradDisp, ('Points', 'velocityDivergence'))

        self.dataGradDisp.RescaleTransferFunctionToDataRange(True)
        self.dataGradDisp.SetScalarBarVisibility(self.renderView, True)

        # show and adjust color bar
        velocityDivLUT  = pv.GetColorTransferFunction('velocityDivergence')
        velocityDivScBr = pv.GetScalarBar(velocityDivLUT,self.renderView)

        scBrTitleFontFamily    = self.fontFamily
        scBrTitleFontSize      = self.scBrTitleFontSize

        scBrLabelRangeFormat   = scBrLabelFormat

        scBrLabelFontFamily    = self.fontFamily
        scBrLabelFontSize      = self.scBrLabelFontSize

        paraViewBasics.SetScalarBarSettings(velocityDivScBr                   ,
                                    title              = scBrTitle              ,
                                    title2             = scBrTitle2             ,
                                    titleFontFamily    = scBrTitleFontFamily    ,
                                    titleFontSize      = scBrTitleFontSize      ,
                                    titleJustification = scBrTitleJustification ,
                                    position           = scBrPosition           ,
                                    position2          = scBrPosition2          ,
                                    orientation        = scBrOrientation        ,
                                    labelNumberOf      = scBrLabelNumber        ,
                                    labelFormat        = scBrLabelFormat        ,
                                    labelRangeFormat   = scBrLabelRangeFormat   ,
                                    labelFontFamily    = scBrLabelFontFamily    ,
                                    labelFontSize      = scBrLabelFontSize       )

    # ===============================================================


    def CalcDivergence(self, calcDivPosText = [0.68, 0.05], **kwargs):

        velDivIntegrated = paraViewBasics.IntegrateVariables(self.dataGrad, 'velocityDivergence')

        textBox      = pv.Text()
        textBox.Text = "int( div(velocity) ): %6.2f" %velDivIntegrated

        textBoxDisp = pv.Show(textBox, self.renderView)
        pos = calcDivPosText

        fontFamily  = self.fontFamily
        fontSize    = self.fontSize

        paraViewBasics.SetTextBoxDisplay(textBoxDisp,
                                         position   = pos         ,
                                         fontFamily = fontFamily  ,
                                         fontSize   = fontSize    )

    # ===============================================================


    def DivergenceIsosurfaces(self,
                              isosurfNumber  = 10,
                              isosurfColor   = [0.0, 0.0, 0.0],
                              isosurfOpacity = 0.65,
                              **kwargs):

        # create contour plot for level set function
        contourDivVel                = pv.Contour(Input=self.dataGrad)
        contourDivVel.ContourBy      = ['POINTS', 'velocityDivergence']

        divVelMin, divVelMax = paraViewBasics.GetRangeOfPointDataField(self.dataGrad, 'velocityDivergence')

        isobars = np.linspace(divVelMin, divVelMax, isosurfNumber)
        contourDivVel.Isosurfaces    = isobars

        # show zero level set
        contourDivVelDisp              = pv.Show(contourDivVel, self.renderView)
        contourDivVelDisp.LineWidth    = self.lineWidth
        contourDivVelDisp.DiffuseColor = isosurfColor
        contourDivVelDisp.Opacity      = isosurfOpacity
        pv.ColorBy(contourDivVelDisp, None)


        pv.SetActiveSource(self.data)

    # ===============================================================


    def VorticityField(self,
                       scBrTitle              = 'vorticity',
                       scBrTitle2             = '[1/s]',
                       scBrTitleJustification = 'Centered',
                       scBrPosition           = None,
                       scBrPosition2          = None,
                       scBrOrientation        = None,
                       scBrLabelNumber        = 3,
                       scBrLabelFormat        = '% 5.2f',
                       **kwargs):

        self.CalcGradient('velocity')

        # show velocity gradient
        pv.ColorBy(self.dataGradDisp, ('Points', 'velocityRotation'))

        self.dataGradDisp.RescaleTransferFunctionToDataRange(True)
        self.dataGradDisp.SetScalarBarVisibility(self.renderView, True)

        # show and adjust color bar
        velocityRotLUT  = pv.GetColorTransferFunction('velocityRotation')
        velocityRotScBr = pv.GetScalarBar(velocityRotLUT,self.renderView)

        scBrTitleFontFamily    = self.fontFamily
        scBrTitleFontSize      = self.scBrTitleFontSize

        scBrLabelRangeFormat   = scBrLabelFormat

        scBrLabelFontFamily    = self.fontFamily
        scBrLabelFontSize      = self.scBrLabelFontSize

        paraViewBasics.SetScalarBarSettings(velocityRotScBr                   ,
                                    title              = scBrTitle              ,
                                    title2             = scBrTitle2             ,
                                    titleFontFamily    = scBrTitleFontFamily    ,
                                    titleFontSize      = scBrTitleFontSize      ,
                                    titleJustification = scBrTitleJustification ,
                                    position           = scBrPosition           ,
                                    position2          = scBrPosition2          ,
                                    orientation        = scBrOrientation        ,
                                    labelNumberOf      = scBrLabelNumber        ,
                                    labelFormat        = scBrLabelFormat        ,
                                    labelRangeFormat   = scBrLabelRangeFormat   ,
                                    labelFontFamily    = scBrLabelFontFamily    ,
                                    labelFontSize      = scBrLabelFontSize       )

    # ===============================================================




    def MomentumField(self,
                      scBrTitle              = 'momentum mag.',
                      scBrTitle2             = '[ N s/m$^3$]',
                      scBrTitleJustification = 'Centered',
                      scBrPosition           = None,
                      scBrPosition2          = None,
                      scBrOrientation        = None,
                      scBrLabelNumber        = 3,
                      scBrLabelFormat        = '% 5.0f',
                      **kwargs):

        self.momentum                 = pv.Calculator(Input=self.data)
        self.momentum.ResultArrayName = 'momentum'
        self.momentum.Function        = 'rho * velocity'

        self.momentumDisp = pv.Show(self.momentum, self.renderView)

        pv.ColorBy(self.momentumDisp, ('POINTS', 'momentum'))
        self.momentumDisp.RescaleTransferFunctionToDataRange(True)
        self.momentumDisp.SetScalarBarVisibility(self.renderView, True)

        momentumLUT = pv.GetColorTransferFunction('momentum')
        momentumPWF = pv.GetOpacityTransferFunction('momentum')

        momentumScBr = pv.GetScalarBar(momentumLUT,self.renderView)

        scBrTitleFontFamily    = self.fontFamily
        scBrTitleFontSize      = self.scBrTitleFontSize

        scBrLabelRangeFormat   = scBrLabelFormat

        scBrLabelFontFamily    = self.fontFamily
        scBrLabelFontSize      = self.scBrLabelFontSize

        paraViewBasics.SetScalarBarSettings(momentumScBr                                ,
                                            title              = scBrTitle              ,
                                            title2             = scBrTitle2             ,
                                            titleFontFamily    = scBrTitleFontFamily    ,
                                            titleFontSize      = scBrTitleFontSize      ,
                                            titleJustification = scBrTitleJustification ,
                                            position           = scBrPosition           ,
                                            position2          = scBrPosition2          ,
                                            orientation        = scBrOrientation        ,
                                            labelNumberOf      = scBrLabelNumber        ,
                                            labelFormat        = scBrLabelFormat        ,
                                            labelRangeFormat   = scBrLabelRangeFormat   ,
                                            labelFontFamily    = scBrLabelFontFamily    ,
                                            labelFontSize      = scBrLabelFontSize       )

    # ===============================================================


    def EnergyField(self,
                    scBrTitle              = 'energy',
                    scBrTitle2             = '[ J/m$^3$]',
                    scBrTitleJustification = 'Centered',
                    scBrPosition           = None,
                    scBrPosition2          = None,
                    scBrOrientation        = None,
                    scBrLabelNumber        = 3,
                    scBrLabelFormat        = '% 5.0f',
                    **kwargs):

        self.energy                 = pv.Calculator(Input=self.data)
        self.energy.ResultArrayName = 'energy'
        self.energy.Function        = '0.5 * rho * ( sqrt(velocity_X^2 + velocity_Y^2 + velocity_Z^2) )^2'

        self.energyDisp = pv.Show(self.energy, self.renderView)

        pv.ColorBy(self.energyDisp, ('POINTS', 'energy'))
        self.energyDisp.RescaleTransferFunctionToDataRange(True)
        self.energyDisp.SetScalarBarVisibility(self.renderView, True)

        energyLUT = pv.GetColorTransferFunction('energy')
        energyPWF = pv.GetOpacityTransferFunction('energy')

        energyScBr = pv.GetScalarBar(energyLUT,self.renderView)

        scBrTitleFontFamily    = self.fontFamily
        scBrTitleFontSize      = self.scBrTitleFontSize

        scBrLabelRangeFormat   = scBrLabelFormat

        scBrLabelFontFamily    = self.fontFamily
        scBrLabelFontSize      = self.scBrLabelFontSize

        paraViewBasics.SetScalarBarSettings(energyScBr                                ,
                                            title              = scBrTitle              ,
                                            title2             = scBrTitle2             ,
                                            titleFontFamily    = scBrTitleFontFamily    ,
                                            titleFontSize      = scBrTitleFontSize      ,
                                            titleJustification = scBrTitleJustification ,
                                            position           = scBrPosition           ,
                                            position2          = scBrPosition2          ,
                                            orientation        = scBrOrientation        ,
                                            labelNumberOf      = scBrLabelNumber        ,
                                            labelFormat        = scBrLabelFormat        ,
                                            labelRangeFormat   = scBrLabelRangeFormat   ,
                                            labelFontFamily    = scBrLabelFontFamily    ,
                                            labelFontSize      = scBrLabelFontSize       )

    # ===============================================================


    def EnergyIsosurfaces(self,
                          isosurfNumber  = 10,
                          isosurfColor   = [0.0, 0.0, 0.0],
                          isosurfOpacity = 0.65,
                          **kwargs):

        # create contour plot for level set function
        contourEnergy                = pv.Contour(Input=self.energy)
        contourEnergy.ContourBy      = ['POINTS', 'energy']

        energyMin, energyMax = paraViewBasics.GetRangeOfPointDataField(self.energy, 'energy')

        isobars = np.linspace(energyMin, energyMax, isosurfNumber)
        contourEnergy.Isosurfaces    = isobars

        # show zero level set
        contourEnergyDisp              = pv.Show(contourEnergy, self.renderView)
        contourEnergyDisp.LineWidth    = self.lineWidth
        contourEnergyDisp.DiffuseColor = isosurfColor
        contourEnergyDisp.Opacity      = isosurfOpacity
        pv.ColorBy(contourEnergyDisp, None)


        pv.SetActiveSource(self.data)


    # ===============================================================




    def PressureField(self,
                      scBrTitle              = 'pressure',
                      scBrTitle2             = '[ N/m$^2$]',
                      scBrTitleJustification = 'Centered',
                      scBrPosition           = None,
                      scBrPosition2          = None,
                      scBrOrientation        = None,
                      scBrLabelNumber        = 3,
                      scBrLabelFormat        = '% 5.0f',
                      **kwargs):

        # # show data
        # self.dataDisp = pv.Show(self.data, self.renderView)

        # show velocity data
        pv.ColorBy(self.dataDisp, ('Points', 'pressure'))
        self.dataDisp.RescaleTransferFunctionToDataRange(True)
        self.dataDisp.SetScalarBarVisibility(self.renderView, True)

        # show and adjust color bar
        pressureLUT = pv.GetColorTransferFunction('pressure')

        # Use parula color map for pressure visualisation
        # cMap = paraViewBasics.CreateColorMapTable(self.data, 'pressure', cMapType='parula', cMapOrdering = 'inverse')
        # pressureLUT.RGBPoints = cMap

        self.pressureScBr = pv.GetScalarBar(pressureLUT,self.renderView)

        scBrTitleFontFamily    = self.fontFamily
        scBrTitleFontSize      = self.scBrTitleFontSize

        scBrLabelRangeFormat   = scBrLabelFormat

        scBrLabelFontFamily    = self.fontFamily
        scBrLabelFontSize      = self.scBrLabelFontSize

        paraViewBasics.SetScalarBarSettings(self.pressureScBr                           ,
                                            title              = scBrTitle              ,
                                            title2             = scBrTitle2             ,
                                            titleFontFamily    = scBrTitleFontFamily    ,
                                            titleFontSize      = scBrTitleFontSize      ,
                                            titleJustification = scBrTitleJustification ,
                                            position           = scBrPosition           ,
                                            position2          = scBrPosition2          ,
                                            orientation        = scBrOrientation        ,
                                            labelNumberOf      = scBrLabelNumber        ,
                                            labelFormat        = scBrLabelFormat        ,
                                            labelRangeFormat   = scBrLabelRangeFormat   ,
                                            labelFontFamily    = scBrLabelFontFamily    ,
                                            labelFontSize      = scBrLabelFontSize       )

    # ===============================================================


    def PressureIsolines(self, isobarNumber  = 10,
                               isobarColor   = [0.0, 0.0, 0.0],
                               isobarOpacity = 0.65,
                               **kwargs):

        # create contour plot for level set function
        self.contourPressure                = pv.Contour(Input=self.data)
        self.contourPressure.ContourBy      = ['POINTS', 'pressure']

        pressureMin, pressureMax = paraViewBasics.GetRangeOfPointDataField(self.data, 'pressure')

        isobars = np.linspace(pressureMin, pressureMax, isobarNumber)
        self.contourPressure.Isosurfaces    = isobars

        # show zero level set
        self.contourPressureDisp              = pv.Show(self.contourPressure, self.renderView)
        self.contourPressureDisp.LineWidth    = self.lineWidth
        self.contourPressureDisp.DiffuseColor = isobarColor
        self.contourPressureDisp.Opacity      = isobarOpacity
        pv.ColorBy(self.contourPressureDisp, None)


        pv.SetActiveSource(self.data)


    # ===============================================================


    # ===============================================================
    # end | Methods
    # ===============================================================

# ===================================================================
# end | class SingleFluidFlowAnalysis
# ===================================================================
