# Strömungsanalyse mittels Paraview Python-API (*pvpython*) und Wrapper-Modulen *parfloan* und *flowanalysis*

Zusammenfassung der Objekt-Attribute.


#### *class*  **parfloan.basics.DataAnalysis(object)** :: *ObjectAttributes*

    * init()
        * self.loadPath
        * self.loadName
        * self.savePath
        * self.saveName
        * self.saveNameFigure
        * self.saveDataType

        * self.timeInstant

        * self.magFac

        * self.lineWidth
        * self.fontFamily
        * self.fontSize

        * self.headingText
        * self.headingPosition

        * self.description

    * CreateView()
        * self.breite
        * self.hoehe

        * self.viewSize

        * self.camPos
        * self.camFocPoint
        * self.camParScal

        * self.renderView

    * LoadData()
        * self.data
        * self.animationScene

    * InitializeViewAndLoadData()
        * self.dataDisp

    * CreateTitle()
        * self.heading
        * self.headingDisp

        * self.subHeading
        * self.subHeadingDisp

    * CreateTimeAnnotation()
            * self.time
            * self.timeDisp

    * CreateDescriptionCalcExample()
        * self.calcExamplDescrpBox
        * self.calcExamplDescrpBoxDisp

        * self.calcExamplDescrpBox2
        * self.calcExamplDescrpBox2Disp




#### *class* **parfloan.onefluid.SingleFluidFlowAnalysis(DataAnalysis)** :: *ObjectAttributes*
    * init()
        * self.scBrTitleFontSize
        * self.scBrLabelFontSize

    * VelocityField()
        * self.velocityScBr

    * Streamlines()
        * self.streamlines
        * self.streamlinesDisp

    * CalcGradient()
        * self.dataGrad
        * self.dataGradDisp

    * PressureField()
        * self.pressureScBr

    * PressureIsolines()
        * self.contourPressure
        * self.contourPressureDisp

    * MomentumField()
        * self.momentum
        * self.momentumDisp

    * EnergyField()
        * self.energy
        * self.energyDisp




#### *class* **parfloan.twofluid.TwoFluidFlowAnalysis(SingleFluidFlowAnalysis)** :: *ObjectAttributes*

    * LevelSetField()
        * self.levelSetScBr

    * LevelSetIsolines()
        * self.contourLS
        * self.contourLSDisp

    * DensityField()
        * self.densityScBr

    * ViscosityField()
        * self.viscosityScBr




#### *class* **ControlGateFlowAnalysis(TwoFluidFlowAnalysis)** :: *ObjectAttributes*

    * ---




#### *class* **ControlGateFlowAnalysisOverview(ControlGateFlowAnalysis)** :: *ObjectAttributes*
    * init()
        * self.figRatio
        * self.camPos
        * self.camFocPoint
        * self.camParScal
        * self.scalBarPos
        * self.scalBarPos2
        * self.scalBarOrient
        * self.tPos
        * self.descrpBoxPos
        * self.descrpBox2Pos
        * self.streamlineStart
        * self.streamlineEnd

    * Show*()
        * self.subHeadPos




#### *class* **ControlGateFlowAnalysisGateVicinity(ControlGateFlowAnalysis)** :: *ObjectAttributes*

    **!! Noch nicht bearbeitet !!**
