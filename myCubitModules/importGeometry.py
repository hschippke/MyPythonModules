"""
Module containing Cubit commands for importing a geometry from a *.sat file
which has been previously generated usin a cad program.
"""

"""
created  by Hans-Henning Schippke
created  on 20.08.15
modified on 23.01.17
"""

import numpy  as np
import cubit
from   basics import cmd


# ===================================================================
# Functions
# ===================================================================

def MergeAll():
    cmd("merge vertex all")
    cmd("merge curve all")
    cmd("merge surface all")
    cmd("merge volume all")


def LoadCadData(inputPath, inputFile):
    """
    Function loading sat-file with name 'inputFile' from path specified
    via 'inputPath'.
    """

    # Einlesen CAD-Datei
    inputData = ' "' + inputPath + inputFile + '" '
    cmd('import acis' + inputData + 'attributes_on  separate_bodies')

    MergeAll()


# ===================================================================

def LoadCubData(inputPath, inputFile):
    """
    Function loading cub-file with name 'inputFile' from path specified
    via 'iinputPath'.
    """
    # Einlesen CAD-Datei
    inputData = ' "' + inputPath + inputFile + '" '
    cmd('open' + inputData)

# ===================================================================

def ShowCurves(curveIDList):

    curveIDString = ""
    for curveID in curveIDList:
        curveIDString = curveIDString + " " + str(curveID)

    cmd("draw curve" + curveIDString)


def RemoveSurplusSurfaces(coordId='z', coordValue=0, tolGeom=0.001):
    """
    Function removing surplus surfaces in order to reduce a 3D geometry
    to 2D.
    """

    cmd('group "wrongSurfaces" add surface with ' + coordId + '_coord > ' + str(coordValue+tolGeom) + ' or ' + coordId + '_coord < ' + str(coordValue-tolGeom))

    wrongSurfaceGroupID = cubit.get_id_from_name("wrongSurfaces")
    wrongSurfaceIDs     = cubit.get_group_surfaces(wrongSurfaceGroupID)

    for item in wrongSurfaceIDs:
        cmd('separate surface ' + str(item))
        cmd('delete surface '   + str(item))

    cubit.delete_group(wrongSurfaceGroupID)

    cmd('merge all')

# ===================================================================


def GetRemainingSurfaceId():
    """
    Function getting the remaining surface IDs (after removing surplus surfaces).
    """

    # Flaeche
    surfaceId = cubit.get_entities("surface")
    #surfaceId = surfaceId[0]

    return surfaceId

# ===================================================================


def RotateSurface(surfaceIDList, axisID, angle):
    """
    Function rotating the surface(s) specified via 'surfaceIDList' around
    axis 'axisID' with angle 'angle'.
    """

    if surfaceIDList == 'all':

        # cmd('rotate surface ' + surfaceIDList + ' about ' + axisID + ' angle ' + str(angle) + 'include_merged')
        cmd('rotate surface ' + surfaceIDList + ' about ' + axisID + ' angle ' + str(angle) )

    elif type(surfaceIDList) == list or type(surfaceIDList) == tuple:

        for surfaceID in surfaceIDList:
            cmd('rotate surface ' + str(surfaceID) + ' about ' + axisID + ' angle ' + str(angle))

# ===================================================================


def CheckCorrectSurfaceNormal(surfaceIDList, direction='z', orientation='positive'):
    """
    Function checking the orientation of the surface for the given 'surfaceID'
    and correcting it if necessary.
    """

    # Normalenrichtung ueberpruefen
    # CUBIT> draw surface all normal

    directionDict = {'x': 0, 'y': 1, 'z': 2}

    if surfaceIDList == 'all':

        surfaceIDTuple = cubit.get_entities("surface")

        for surfaceID in surfaceIDTuple:
            ChangeOrientationSurfaceNormal(surfaceID, direction, orientation, directionDict)

    elif type(surfaceIDList) == tuple or type(surfaceIDList) == list:

        for surfaceID in surfaceIDList:
            ChangeOrientationSurfaceNormal(surfaceID, direction, orientation, directionDict)

# ===================================================================


def ChangeOrientationSurfaceNormal(surfaceID, direction, orientation, directionDict={"x": 0, "y": 1, "z": 2}):
    """
    Function adjusting the surface normal of the surface specified via 'surfaceID'
    for the specified 'direction' (x, y, z) into the specified 'orientation'.
    """

    surfaceNormal = cubit.get_surface_normal(surfaceID)
    surfaceNormal = list(surfaceNormal)

    if orientation == "positive":

        if surfaceNormal[directionDict[direction]] < 0:

            cmd("reverse surface " + str(surfaceID))

    elif orientation == "negative":

        if surfaceNormal[directionDict[direction]] > 0:

            cmd("reverse surface " + str(surfaceID))


# ===================================================================
# ===================================================================


def GetSurfaceGeometryData(tolGeom, surfaceIDList=None):
    """
    Function generating a dict with geometry information for the surfaces
    specified via their surfaceID in surfaceIDList.
    If surfaceIDList is not provided, a dict is created containing
    geometry information of all existing surfaces.
    The dicts belonging to each surface are summarised in a list
    which is returned.


    Input data:
    --------------

    surfaceIDList: list (or tuple) with surfaceIDs.
                   If not provided, it will be created.

    tolGeom:       float used to define search values to find nodes on the boundaries.


    Return data:
    ---------------

    surfaceGeometryList: list of dicts
                         containing geometry information related to each surface
                         [ { 'surfaceID',
                             'xMin, xMax', 'yMin', 'yMax',
                             'boundLeft', 'boundRight', 'boundTop', 'boundBot' } ]


    Usage:
    ---------

    surfaceGeometryList = GetSurfaceGeometryData(tolGeom, surfaceIDList=None)

    """

    surfaceGeometryList = [] # initialise return list

    if surfaceIDList is None:
        surfaceIDList = cubit.get_entities("surface")

    for surface in surfaceIDList:

        # Auslesen Geometrie-Daten Flaeche
        geometrySurface = cubit.get_bounding_box("surface", surface)

        surfaceGeometry = {"surfaceID" : surface             ,
                           "xMin"      : geometrySurface[0]  ,
                           "xMax"      : geometrySurface[1]  ,
                           "yMin"      : geometrySurface[3]  ,
                           "yMax"      : geometrySurface[4]  }

        surfaceGeometry["boundLeft"]  = surfaceGeometry["xMin"] + tolGeom
        surfaceGeometry["boundRight"] = surfaceGeometry["xMax"] - tolGeom
        surfaceGeometry["boundBot"]   = surfaceGeometry["yMin"] + tolGeom
        surfaceGeometry["boundTop"]   = surfaceGeometry["yMax"] - tolGeom

        surfaceGeometryList.append(surfaceGeometry)


    return surfaceGeometryList


# ===================================================================


def GetCurveGeometryData(surfaceGeometryList):
    """
    Function generating a list of dicts containing geometry information for
    each curve related to the surface specified via 'surfaceGeometryList'.


    Input data:
    --------------

    surfaceGeometryList: list of dicts
                         every dict must have a key 'surfaceID' specifying
                         the surfaceID of each surface


    Return data:
    ---------------

    curveGeometry: list of dicts
                   The list contains one dict for every curve bounding the surface.
                   Each dict contains geometry information for one curve.
                   [ { 'curveID',
                       'xMin', 'xMax'  , 'yMin', 'yMax',
                       'vertexA_Id'    , 'vertexB_Id',
                       'vertexA_xCoord', 'vertexA_yCoord',
                       'vertexB_xCoord', 'vertexB_yCoord'  } ]


    Usage:
    ---------

    curveGeometry = GetCurveGeometryData(surfaceGeometryList)

    """

    curveGeometry = list() # initialise return list
    curveIDList   = list() # auxiliary list containing curve IDs only;
                           # used for preventing curves already contained in
                           # curveGeometry added again

    # extract surface IDs from surfaceGeometryList
    surfaceIDList = list()
    for surface in surfaceGeometryList:
        surfaceIDList.append(surface['surfaceID'])


    # extract geometry information of each curve
    for surfaceID in surfaceIDList:

        curveIDTuple = cubit.get_relatives("surface",surfaceID,"curve")

        for ii, currCurveID in enumerate(curveIDTuple):

            if currCurveID not in curveIDList:

                curveIDList.append(currCurveID)

                geometryCurves  = cubit.get_bounding_box("curve", currCurveID)

                vertexIDs       = cubit.get_relatives("curve",currCurveID,"vertex")
                geometryVertexA = cubit.get_bounding_box("vertex",vertexIDs[0])
                geometryVertexB = cubit.get_bounding_box("vertex",vertexIDs[1])

                curveGeometry.append({"curveID"        : currCurveID        ,
                                      "xMin"           : geometryCurves[0]  ,
                                      "xMax"           : geometryCurves[1]  ,
                                      "yMin"           : geometryCurves[3]  ,
                                      "yMax"           : geometryCurves[4]  ,
                                      "vertexA_Id"     : vertexIDs[0]       ,
                                      "vertexB_Id"     : vertexIDs[1]       ,
                                      "vertexA_xCoord" : geometryVertexA[0] ,
                                      "vertexA_yCoord" : geometryVertexA[3] ,
                                      "vertexB_xCoord" : geometryVertexB[0] ,
                                      "vertexB_yCoord" : geometryVertexB[3] })

    return curveGeometry

# ===================================================================


def GetVertexGeometryData(surfaceGeometryList):
    """
    Function generating a list of dicst containing geometry information for
    every vertex related to the surface specified via 'surfaceGeometryList'.


    Input data:
    --------------

    surfaceGeometryList: list of dicts
                         every dict must have a key 'surfaceID' specifying
                         the surfaceID of each surface


    Return data:
    ---------------

    vertexGeometry: list of dicts
                    The list contains one dict for every vertex belonging to the surface.
                    Each dict contains geometry information for that vertex.
                    [ { 'vertexID', 'xCoord', 'yCoord', 'zCoord'}]


    Usage:
    ---------

    vertexGeometry = GetVertexGeometryData(surfaceGeometryList)

    """


    vertexGeometry = []     # initialise return list
    vertexIDList   = list() # auxiliary list containing vertex IDs only;
                            # used for preventing vertices already contained in
                            # vertexGeometry added again

    # extract surface IDs from surfaceGeometryList
    surfaceIDList = list()
    for surface in surfaceGeometryList:
        surfaceIDList.append(surface['surfaceID'])


    # extract geometry information of each curve
    for surfaceID in surfaceIDList:

        vertexIDTuple = cubit.get_relatives('surface', surfaceID, 'vertex')

        for vertexID in vertexIDTuple:

            if vertexID not in vertexIDList:

                vertexIDList.append(vertexID)

                coordinates = cubit.get_bounding_box('vertex', vertexID)

                vertexGeometry.append( {'vertexID' : vertexID      ,\
                                        'xCoord'   : coordinates[0],\
                                        'yCoord'   : coordinates[3],\
                                        'zCoord'   : coordinates[6] } )

    return vertexGeometry

# ===================================================================


def isInSearchBox(searchBox, xCoord, yCoord):
    """
    Function if an entity (surface, curve, vertex) specified via two lists
    of coordinates 'xCoord' and 'yCoord' are inside a rectangular area
    defined via 'searchBox'.
    If the entity is completely inside the search box the function returns True
    otherwiese False.
    """

    returnValue = False

    if xCoord.min() >= searchBox['x'].min() and xCoord.max() <= searchBox['x'].max():
        if yCoord.min() >= searchBox['y'].min() and yCoord.max() <= searchBox['y'].max():

            returnValue = True


    return returnValue

# ===================================================================

def GetSurfaceInSearchBox(searchBox, surfaceListDict):
    """
    Function returning a list of dicts in which each dict contains
    geometry information of the surfaces which are completely inside
    the searchBox. The returned list of dicts is a subset of 'surfaceListDict'.
    """

    surfaceInSearchBox = list()

    for surface in surfaceListDict:
        xCoord = np.array([surface['xMin'], surface['xMax']])
        yCoord = np.array([surface['yMin'], surface['yMax']])

        if isInSearchBox(searchBox, xCoord, yCoord):

            surfaceInSearchBox.append(surface)

    return surfaceInSearchBox


def GetCurvesInSearchBox(searchBox, curveListDict):
    """
    Function returning a list of dicts in which each dict contains
    geometry information of the curves which are completely inside
    the searchBox. The returned list of dicts is a subset of 'curveListDict'.
    """

    curvesInSearchBox = list()

    for curve in curveListDict:
        xCoord = np.array([curve["vertexA_xCoord"], curve["vertexB_xCoord"]])
        yCoord = np.array([curve["vertexA_yCoord"], curve["vertexB_yCoord"]])

        if isInSearchBox(searchBox, xCoord, yCoord):

            curvesInSearchBox.append(curve)


    return curvesInSearchBox

# ===================================================================


def GetVerticesInSearchBox(searchBox, vertexListDict):
    """
    Function returning a list of dicts in which each dict contains
    geometry information of the vertices which are completely inside
    the searchBox. The returned list of dicts is a subset of 'vertexListDict'.
    """

    verticesInSearchBox = []

    for vertex in vertexListDict:
        xCoord = np.array([ vertex['xCoord'] ])
        yCoord = np.array([ vertex['yCoord'] ])

        if isInSearchBox(searchBox, xCoord, yCoord):

            verticesInSearchBox.append(vertex)


    return verticesInSearchBox

# ===================================================================
# ===================================================================

def GetSurfaceIDsFromGeometry(overallSurfaceList, surfaceGeometry):
    """
    Function getting the surfaceIDs for each entity/dict in 'overallSurfaceList'
    via their searchBox from surfaceGeometry.

    Each dict in 'overallSurfaceList' is extended with the key 'surfaceID'.


    Input data:
    --------------

    overallSurfaceList: list of dicts
                        Each dict contains geometry information of one surface.
                        [ {'searchBox', ... } ]

    surfaceGeometry: list of dicts
                     The list contains one dict for each surface.
                     Each dict contains geometry information for one surface.
                     [ { 'surfaceID',
                         'xMin, xMax', 'yMin', 'yMax',
                         'boundLeft', 'boundRight', 'boundTop', 'boundBot' } ]


    Output data:
    ---------------

    overallSurfaceList


    Usage:
    ---------

    GetSurfaceIDsFromGeometry(overallSurfaceList, surfaceGeometry)

    """

    for surfaceDict in overallSurfaceList:

        searchBox = surfaceDict['searchBox']

        surfaceGeomListDict = GetSurfaceInSearchBox(searchBox, surfaceGeometry)

        surfaceIDList = []
        for item in surfaceGeomListDict:
            surfaceIDList.append(item['surfaceID'])

        surfaceDict.update({'surfaceID' : surfaceIDList})

    return overallSurfaceList


def GetCurveIDsFromGeometry(overallCurveList, curveGeometry):
    """
    Function getting the curveIDs for each entity/dict in 'overallCurveList'
    via their searchBox from curveGeometry.

    Each dict in 'overallCurveList' is extended with the key 'curveID'.

    In case the gained curveID list related to the searchBox has to be reduced
    by curve IDs already belonging to other entities of 'overallCurveList',
    the list belonging to the key 'compareReduce' is nonempty and contains the
    entities against which to compare and to reduce.


    Input data:
    --------------

    overallCurveList: list of dicts
                      Each dict contains geometry information of one curve.
                      [ {'searchBox', 'compareReduce', ... } ]

    curveGeometry: list of dicts
                   The list contains one dict for every curve bounding the surface.
                   Each dict contains geometry information for one curve.
                   [ { 'curveID',
                       'xMin', 'xMax'  , 'yMin', 'yMax',
                       'vertexA_Id'    , 'vertexB_Id',
                       'vertexA_xCoord', 'vertexA_yCoord',
                       'vertexB_xCoord', 'vertexB_yCoord'  } ]


    Output data:
    ---------------

    overallCurveList


    Usage:
    ---------

    GetCurveIDsFromGeometry(overallCurveList, curveGeometry)

    """

    """
    TODO:
    - Ueberoruefung, ob compareReduce-Abschnitt korrekt funktioniert, insbes. ob
      curveGeomListDict_bPTC    = [{'curveID' : boundaryPartToCompare['curveID'][0]}]
      oder eigentlich
      curveGeomListDict_bPTC    = [{'curveID' : boundaryPartToCompare['curveID']}]
      sein muesste.
    """

    for curveDict in overallCurveList:

        searchBox = curveDict['searchBox']

        curveGeomListDict = GetCurvesInSearchBox(searchBox, curveGeometry)

        # if list 'compareReduce' has entries: reduce curveGeomListDict
        if curveDict['compareReduce']:

            for nameBoundaryPartToCompare in curveDict['compareReduce']:
                boundaryPartToCompare     = [ element for element in overallCurveList if element['name'] == nameBoundaryPartToCompare ]
                boundaryPartToCompare     = boundaryPartToCompare[0]

                curveGeomListDict_bPTC    = [{'curveID' : boundaryPartToCompare['curveID'][0]}]

                curveGeomListDict         = CompareReduceListDict(curveGeomListDict_bPTC, curveGeomListDict, 'curveID')

        curveIdList = []
        for item in curveGeomListDict:
            curveIdList.append(item['curveID'])

        # curveID -> curves
        curveDict.update({'curveID' : curveIdList})

    return overallCurveList


# ===================================================================


def GetVertexIDsFromGeometry(overallVertexList, vertexGeometry):
    """
    Function getting the vertexIDs for each entity/dict in 'overallVertexList'
    via their searchBox from vertexGeometry.

    Each dict in 'overallVertexList' is extended with the key 'vertexID'.

    In case the gained vertexID list related to the searchBox has to be reduced
    by vertex IDs already belonging to other entities of 'overallVertexList',
    the list belonging to the key 'compareReduce' is nonempty and contains the
    entities against which to compare and to reduce.


    Input data:
    --------------

    overallVertexList: list of dicts
                       Each dict contains geometry information of one vertex.
                       [ {'searchBox', 'compareReduce', ... } ]

    vertexGeometry: list of dicts
                    The list contains one dict for every vertex belonging to the surface.
                    Each dict contains geometry information for that vertex.
                    [ { 'vertexID', 'xCoord', 'yCoord', 'zCoord'}]


    Output data:
    ---------------

    overallVertexList


    Usage:
    ---------

    GetVertexIDsFromGeometry(overallVertexList, vertexGeometry)

    """

    """
    TODO:
    - Ueberpruefung, ob compareReduce-Abschnitt korrekt funktioniert, insbes. ob
      vertexGeomListDict_vertToComp = [{'vertexID' : vertexToCompare['vertexID'][0]}]
      oder eigentlich
      vertexGeomListDict_vertToComp = [{'vertexID' : vertexToCompare['vertexID']c}]
      sein muesste.
    """

    for vertexDict in overallVertexList:

        searchBox = vertexDict['searchBox']

        vertexGeomListDict = GetVerticesInSearchBox(searchBox, vertexGeometry)

        if vertexDict['compareReduce']:

            for nameVertexToCompare in vertexDict['compareReduce']:
                vertexToCompare = [ element for element in overallVertexList if element['name'] == nameVertexToCompare ]
                vertexToCompare = vertexToCompare[0] # reduce list of single dict to dict only

                # altes Vorgehen; beruecksichtigt vorausgegangene reduceCompate-Schritte nicht
                #searchBox_vertToComp = vertexToCompare['searchBox']
                #vertexGeomListDict_vertToComp = GetVerticesInSearchBox(searchBox_vertToComp, vertexGeometry)

                # neues Vorgehen:
                vertexGeomListDict_vertToComp = [{'vertexID' : vertexToCompare['vertexID'][0]}]
                vertexGeomListDict = CompareReduceListDict(vertexGeomListDict_vertToComp, vertexGeomListDict, 'vertexID')

        vertexIDList = []

        for vertex in vertexGeomListDict:
            vertexIDList.append(vertex['vertexID'])

        vertexDict.update({'vertexID' : vertexIDList})

    return overallVertexList


# ===================================================================


def CompareReduceListDict(dict1, dict2, entry):
    """
    Function comparing two dicts with each other and reduces duplicate entries
    from dict2 which also occur in dict 1.
    Compared are lists of both dict with key 'entry'.

    Therefore: len(dict2) !> len(dict1)

    dict2 is returned reduced by the entries of dict1.
    """

    entryValueDict1 = list()
    for item in dict1:
        entryValueDict1.append(item[entry])

    entryValueDict2 = list()
    for item in dict2:
        entryValueDict2.append(item[entry])

    entryValueDict1 = set(entryValueDict1)
    entryValueDict2 = set(entryValueDict2)

    entryValueDict2Remain = entryValueDict2 - entryValueDict2.intersection(entryValueDict1)


    entryValueDict1       = list(entryValueDict1)
    entryValueDict2       = list(entryValueDict2)
    entryValueDict2Remain = list(entryValueDict2Remain)

    dict2IndexRemain = list()
    for item in entryValueDict2Remain:
        dict2IndexRemain.append(entryValueDict2.index(item))

    dict2New = list()
    for item in dict2IndexRemain:
        dict2New.append(dict2[item])


    return dict2New

# ===================================================================


def ParseIDs2BoundaryConditionList(boundaryConditionList, vertices, curves):
    """
    Function parsing curve and vertex IDs from 'vertices' and 'curves'
    to 'boundaryConditionList'.
    """

    for bcDict in boundaryConditionList:

        bcDictEntities = bcDict['entites']

        if 'vertices' in bcDictEntities:

            vertexIDList = []

            for vertexName in bcDictEntities['vertices']:
                for vertexDict in vertices:
                    if vertexDict['name'] == vertexName:
                        vertexIDList.append(vertexDict['vertexID'])

            # flatten vertexIDList
            vertexIDList       = [val for sublist in vertexIDList for val in sublist]
            bcDict['vertexID'] = vertexIDList


        if 'curves' in bcDictEntities:

            curveIDList  = []

            for curveName in bcDictEntities['curves']:
                for curveDict in curves:
                    if curveDict['name'] == curveName:
                        curveIDList.append(curveDict['curveID'])

            # flatten curveIDList
            curveIDList       = [val for sublist in curveIDList for val in sublist]
            bcDict['curveID'] = curveIDList

# ===================================================================


# ===================================================================
# end | Functions
# ===================================================================
