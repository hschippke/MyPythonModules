"""
Module containing Cubit commands for meshing geometric entities.
"""

"""
created  by Hans-Henning Schippke
created  on 20.08.15
modified on 20.01.17
"""

import cubit, os
from   basics import cmd


# ===================================================================
# Functions
# ===================================================================

def MeshCurveByInterval(dict, meshInterval, biasfactor=1.0, dualbias=0):
    """
    Function meshing curves using a specified interval.
    The curves to be meshed are defined via a list of dicts 'dict'.
    Each entry must have the key-value pair "curveID".
    The 'meshInterval' specifies the number of intervals along the curve.
    """

    for item in dict:

        cmd('curve ' + str(item['curveID']) + ' interval ' + str(meshInterval))

        if dualbias == 1:
            cmd('curve ' + str(item['curveID']) + ' scheme dualbias factor ' + str(biasfactor))
        else:
            cmd('curve ' + str(item['curveID']) + ' scheme     bias factor ' + str(biasfactor))


        cmd('mesh curve ' + str(item['curveID']))

# ===================================================================


def MeshCurveBySize(dict, meshSize):
    """
    Function meshing curves using a fixed size.
    The curves to be meshed are defined via a list of dicts 'dict'.
    Each entry must have the key-value pair "curveID".
    The 'meshSize' specifies mesh size along the curve.
    """

    for item in dict:
        cubit.silent_cmd('curve '      + str(item['curveID']) + ' size ' + str(meshSize))
        #cubit.silent_cmd('curve '      + str(item['curveID']) + ' scheme equal')
        cubit.silent_cmd('curve '      + str(item['curveID']) + ' interval odd')

        cubit.silent_cmd('mesh curve ' + str(item['curveID']))

# ===================================================================


def MeshAllCurves(curveList):
    """
    Function meshing all curves summarised in 'curveList'.
    Each curve is one dict in the list 'curveList'.

    In case the geometry has been created directly in Cubit,
    each curve dict contains a key 'geomGroupId' which is used
    to get the corresponding curve IDs.

    In case the geometry has been imported from a CAD program via
    a sat-file, the curveIDs are directly stored in each curve dict
    via the key 'curveID'.

    Depending on the key 'meshCurveType' the curves belonging to
    one curve dict are meshed by interval or by size.
    """

    for curve in curveList:

        curveIDListDict = []

        if 'geomGroupId' in curve: # geometry created in Cubit

            curveIdList = cubit.get_group_curves(curve['geomGroupId'])

            for curveID in curveIdList:
                curveIDListDict.append({'curveID' : curveID})

        elif 'curveID' in curve: # geometry imported form CAD file

            for curveID in curve['curveID']:
                curveIDListDict.append({'curveID' : curveID})


        if curve['meshCurveType'] == 'size':
            MeshCurveBySize(curveIDListDict, curve['meshSize'])

        if curve['meshCurveType'] == 'interval':
            if 'biasFactor' in curve:
                if 'dualbias' in curve:
                    MeshCurveByInterval(curveIDListDict, curve['meshInterval'], biasfactor=curve['biasFactor'], dualbias=curve['dualbias'])
                else:
                    MeshCurveByInterval(curveIDListDict, curve['meshInterval'], biasfactor=curve['biasFactor'])
            else:
                MeshCurveByInterval(curveIDListDict, curve['meshInterval'])

# ===================================================================

def ConcatenateCurvesIDLists(curveIDSuperList):

    curves = []

    for lst in curveIDSuperList:
        for dct in lst:
            if dct not in curves:
                curves.append(dct)

    return curves

# ===================================================================
# ===================================================================


def MeshSurface(surfDict, meshScheme='pave', paveCleanUpExtend=0):
    """
    Function meshing surface specified via 'surfaceDict' using scheme 'meshScheme'.
    The dict 'surfaceDict' must have the key-value pair "surfaceID" specifiying the
    surface id.
    """

    cmd('surface ' + str(surfDict['surfaceID']) + ' scheme ' + meshScheme)

    if paveCleanUpExtend == 1:
        cmd('set paver cleanup extend')

    cmd('mesh surface ' + str(surfDict['surfaceID']))

# ===================================================================


def MeshAllSurfaces(surfaceList):
    """
    Function meshing all surfaces defined as dict in the list 'surfaceList'.

    Each surface dict must contain a key 'surfaceID' as well as key
    'meshScheme' and 'meshOptions'.

    The value belonging to 'meshOptions' is itself a dict containing
    surface specific mesh options.
    """

    for surface in surfaceList:

        if type(surface['surfaceID']) is list:
            surfaceIDDict = {'surfaceID' : surface['surfaceID'][0]}
        else:
            surfaceIDDict = {'surfaceID' : surface['surfaceID']}

        meshOptionDict = surface['meshOptions']

        if 'paveCleanUpExtend' in meshOptionDict:
            MeshSurface(surfaceIDDict, surface['meshScheme'], meshOptionDict['paveCleanUpExtend'])
        else:
            MeshSurface(surfaceIDDict, surface['meshScheme'])




# ===================================================================
# ===================================================================


def RenumberAll():
    """
    Function ensuring consectuive renumbering.
    """

    # Konsekutive Nummerierung sicherstellen; beginnend bei 1
    cmd( "renumber node all start_id 1 uniqueids" ) # Knotennummern
    cmd( "renumber edge all start_id 1 uniqueids" ) # Elementkanten
    cmd( "renumber face all start_id 1 uniqueids" ) # Elementnummern (2D)/Elementoberflaechen
    cmd( "renumber hex  all start_id 1 uniqueids" ) # Elementnummern (3D)

# ===================================================================
# ===================================================================


def CreateMeshGroup(groupName, entityType, entityIdList):
    """
    Function creating a mesh group with 'groupName' and adding nodes in vertices
    and/or curves depending on 'entityType' with IDs in entityIdList to that group.
    The groupId is returned.
    """

    cmdString     = 'group "' + groupName + '" add node'

    if entityType == 'curve':

        cmdString = cmdString + ' in curve'

    if entityType == 'vertex':

        cmdString = cmdString + ' in vertex'


    for id in entityIdList:
        cmdString = cmdString + ' ' + str(id)

    cmd(cmdString)

    groupId = cubit.get_id_from_name(groupName)

    return groupId

# ===================================================================


def SubtractGroups(group1ID, group2ID):
    """
    Subtract group2 from group1.
    group1 = group1 - group2
    """

    # intersect mesh groups to remove double nodes at corners
    cmd('group ' + str(group1ID) + ' subtract group ' + str(group2ID) + ' from group ' + str(group1ID) )

# ===================================================================


def CreateMeshGroupListDict(entityListDict):
    """
    Function summarising nodes related to geometry groups of vertices and/or curves
    in a mesh group.


    Input data:
    --------------

    entityListDict: list of dicts
                    {"name", "vertices", "curves", "geomGroupName", "geomGroupId"}


    Output data:
    ---------------

    curveListDict: list of dicts
                    {"name", "vertices", "curves", "geomGroupName", "geomGroupId",
                     "meshGroupName", "meshGroupId"}
    """


    for entityDict in entityListDict:

        geomGroupId   = entityDict["geomGroupId"]

        if 'curves' in entityDict and entityDict["curves"] != []:
            entityIdList   = cubit.get_group_curves(geomGroupId)
            entityType    = 'curve'

        if 'vertices' in entityDict and entityDict["vertices"] != []:
            entityIdList = cubit.get_group_vertices(geomGroupId)
            entityType   = 'vertex'

        meshGroupName = "grMesh_" + entityDict["name"]

        groupId = CreateMeshGroup(meshGroupName, entityType,  entityIdList)

        entityDict.update({"meshGroupName" : meshGroupName, "meshGroupId" : groupId})

        if entityDict['reduceBy']:
            for reducePart in entityDict['reduceBy']:
                reducePartMeshGroupID = cubit.get_id_from_name('grMesh_' + reducePart)

                SubtractGroups(groupId, reducePartMeshGroupID)


# ===================================================================


def CreateMeshGroupsFromBoundaryConditionList(boundaryConditionList):
    """
    Function creating mesh groups for every dict in the list 'boundaryConditionList'.

    If the lists belonging to the keys 'curveID' or 'vertexID' are nonempty,
    a meshgroup is created containing the curves or vertices defined in those lists.

    In case the key 'reduceBy' of a dict is nonempty, the created mesh group
    is reduced by those nodes contained also in the groups named in 'reduceBy'.
    """

    for bcDict in boundaryConditionList:

        meshGroupName = "grMesh_" + bcDict['name']

        if bcDict['curveID'] != []:
            entityType  = 'curve'
            meshGroupID = CreateMeshGroup(meshGroupName, entityType, bcDict['curveID'])

        if bcDict['vertexID'] != []:
            entityType  = 'vertex'
            meshGroupID = CreateMeshGroup(meshGroupName, entityType, bcDict['vertexID'])

        bcDict['meshGroupName'] = meshGroupName
        bcDict['meshGroupID']   = meshGroupID

        # intersect mesh groups to remove double nodes at corners
        if bcDict['reduceBy']:
            for reducePart in bcDict['reduceBy']:
                reducePartMeshGroupID = cubit.get_id_from_name('grMesh_' + reducePart)

                SubtractGroups(meshGroupID, reducePartMeshGroupID)


# ===================================================================
# ===================================================================

def QualityAnalysis(outputPath):

    outputFolder = outputPath + 'MeshQuality'
    try:
        os.makedirs(outputFolder)
    except OSError:
        if not os.path.isdir(outputFolder):
            raise


    # metrices applicable to quadrilateral elements (taken from Cubit 13.2 manual)
    quadrilateralMetricDict = [{'metricName' : "aspect ratio"    , 'saveName' : "AspectRatio"    , 'acceptableRange' : [ 1.0 ,   4.0], 'fullRange' : [  1, float("inf")], 'limit' : "low" },\
                               {'metricName' : "skew"            , 'saveName' : "Skew"           , 'acceptableRange' : [ 0.0 ,   0.5], 'fullRange' : [  0,   1         ], 'limit' : "low" },\
                               #{'metricName' : "taper"           , 'saveName' : "Taper"          , 'acceptableRange' : [ 0.0 ,   0.7], 'fullRange' : [  0, float("inf")], 'limit' : "low" },\
                               #{'metricName' : "warpage"         , 'saveName' : "Warpage"        , 'acceptableRange' : [ 0.9 ,   1.0], 'fullRange' : [  0,   1         ], 'limit' : "high"},\
                               {'metricName' : "stretch"         , 'saveName' : "Stretch"        , 'acceptableRange' : [ 0.25,   1.0], 'fullRange' : [  0,   1         ], 'limit' : "high"},\
                               {'metricName' : "minimum angle"   , 'saveName' : "AngleMin"       , 'acceptableRange' : [45.0 ,  90.0], 'fullRange' : [  0,  90         ], 'limit' : "high"},\
                               {'metricName' : "maximum angle"   , 'saveName' : "AngleMax"       , 'acceptableRange' : [90.0 , 135.0], 'fullRange' : [ 90, 360         ], 'limit' : "low" },\
                               {'metricName' : "condition no."   , 'saveName' : "ConditionNumber", 'acceptableRange' : [ 1.0 ,   4.0], 'fullRange' : [  1, float("inf")], 'limit' : "low" },\
                               {'metricName' : "scaled jacobian" , 'saveName' : "JacobianScaled" , 'acceptableRange' : [ 0.5 ,   1.0], 'fullRange' : [- 1,   1         ], 'limit' : "high"},\
                               {'metricName' : "shear"           , 'saveName' : "Shear"          , 'acceptableRange' : [ 0.3 ,   1.0], 'fullRange' : [  0,   1         ], 'limit' : "high"},\
                               {'metricName' : "shape"           , 'saveName' : "Shape"          , 'acceptableRange' : [ 0.3 ,   1.0], 'fullRange' : [  0,   1         ], 'limit' : "high"},\
                               #{'metricName' : "relative size"   , 'saveName' : "RelativeSize"   , 'acceptableRange' : [ 0.3 ,   1.0], 'fullRange' : [  0,   1         ], 'limit' : "high"},\
                               {'metricName' : "shear and size"  , 'saveName' : "ShearSize"      , 'acceptableRange' : [ 0.2 ,   1.0], 'fullRange' : [  0,   1         ], 'limit' : "high"},\
                               {'metricName' : "shape and size"  , 'saveName' : "ShapeSize"      , 'acceptableRange' : [ 0.2 ,   1.0], 'fullRange' : [  0,   1         ], 'limit' : "high"},\
                               {'metricName' : "distortion"      , 'saveName' : "Distortion"     , 'acceptableRange' : [ 0.6 ,   1.0], 'fullRange' : [- 1,   1         ], 'limit' : "high"},\
                               ]

    outputFolder = outputFolder + '/'

    cubit.reset_camera()

    # element area
    cmd("quality surface all element area global draw mesh")
    cmd("hardcopy  '" + outputFolder + "MeshQuality_ElementArea.jpg" + "' jpg window 1")

    # jacobian
    cmd("quality surface all jacobian global draw mesh")
    cmd("hardcopy  '" + outputFolder + "MeshQuality_Jacobian.jpg" + "' jpg window 1")


    # cmd("quality surface all scaled jacobian global draw mesh")
    # cmd("hardcopy  '" + outputPath + "meshQuality_jacobianScaled.jpg" + "' jpg window 1")

    # cmd("quality surface all condition no. global draw mesh")
    # cmd("hardcopy  '" + outputPath + "meshQuality_conditionNo.jpg" + "' jpg window 1")

    # cmd("quality surface all distortion global draw mesh")
    # cmd("hardcopy  '" + outputPath + "meshQuality_distortion.jpg" + "' jpg window 1")


    for metric in quadrilateralMetricDict:

        cmd("quality surface all " + metric['metricName'] + " global draw mesh")
        cmd("hardcopy '" + outputFolder + "MeshQuality_" + metric['saveName'] + ".jpg" + "' jpg window 1")


        if metric['limit'] == "low":
            limitValue = metric['acceptableRange'][1]
        elif metric['limit'] == "high":
            limitValue = metric['acceptableRange'][0]

        cmd("quality surface all " + metric['metricName'] + " global " + metric['limit'] + " " + str(limitValue) + " draw mesh")
        cmd("hardcopy '" + outputFolder + "MeshQuality_" + metric['saveName'] + "_ElemOutOfRange" + ".jpg" + "' jpg window 1")


    cubit.set_rendering_mode(5) # reset view to original mode

# ===================================================================


# ===================================================================
# end | Functions
# ===================================================================
