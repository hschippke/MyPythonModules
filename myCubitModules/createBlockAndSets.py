"""
Module containing Cubit commands for creating blocks, nodesets and sidesets.
"""

"""
created  by Hans-Henning Schippke
created  on 21.08.15
modified on 21.08.15
"""

import cubit
from   basics import cmd


# ===================================================================
# Functions
# ===================================================================

def CreateBlock(blockId, entityIdList, elementType, elementTypeID, matID, loadID):
    """
    Function creating a block with 'blockId' and 'elementType'.
    Its name is made up of 'elementTypeID', 'matID' and 'loadID'.
    The block contains the surfaces with IDs listes in 'entityIdList'.
    """

    if type(entityIdList) == str:

        cmd('block ' + str(blockId) + ' surface ' + entityIdList)

    else: # entityIdList is a list of integers

        cmdString = 'block ' + str(blockId) + ' surface'

        for entityId in entityIdList:
            cmdString = cmdString + ' ' + str(entityId)

        cmd(cmdString)

    cmd('block ' + str(blockId) + ' element type '        + str(elementType))
    cmd('block ' + str(blockId) + ' name "elementTypeID ' + str(elementTypeID) + \
                                        ' matID '         + str(matID)         + \
                                        ' loadID '        + str(loadID)        + '"')

# ===================================================================


def CreateSideset(id, surfaceIdList):
    """
    Function creating a sideset with 'id' and consisting of the surfaces
    listed in 'surfaceIdList'.
    """

    if type(surfaceIdList) == str:

        cmd('sideset ' + str(id) + ' surface ' + surfaceIdList)

    else: # surfaceIdList is a list of integers

        cmdString = 'sideset ' + str(id) + ' surface'

        for entityId in surfaceIdList:
            cmdString = cmdString + ' ' + str(entityId)

        cmd(cmdString)

# ===================================================================


def CreateNodesetBC(nodeSetId, nodeSetName, meshGroupId):
    """
    Function creating a nodeset with number 'nodeSetId' consisting of the nodes
    summarised in the mesh group with id 'meshGroupId'. The name of the nodeset
    is defined via 'nodeSetName'.
    """

    cmd('nodeset ' + str(nodeSetId) + ' node in group ' + str(meshGroupId))
    cmd('nodeset ' + str(nodeSetId) + ' name "' + nodeSetName + ' ' + str(nodeSetId) + '"')

# ===================================================================


def CreateNodesetIC(nodeSetId, nodeSetName):
    """
    Function creating a nodset with number 'nodeSetId' consisting of all nodes
    in all surfaces. The name of the nodeset is defined via 'nodeSetName'.
    """

    cmd('nodeset ' + str(nodeSetId) + ' node in surface all')
    cmd('nodeset ' + str(nodeSetId) + ' name "' + nodeSetName + ' ' + str(nodeSetId) + '"')

# ===================================================================

# ===================================================================
# end | Functions
# ===================================================================