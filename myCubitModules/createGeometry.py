"""
Module containing Cubit commands for creating a geometry using vertices,
curves and surfaces.
"""

"""
created  by Hans-Henning Schippke
created  on 20.08.15
modified on 01.09.15
"""

import cubit
from   basics import cmd


# ===================================================================
# Functions
# ===================================================================


def CreateVertex(xCoord, yCoord, zCoord):
    """
    Function creating a vertex via its coordinates 'xCoord', 'yCoord', 'zCoord'
    and returning the vertex ID.
    """

    cmd("create vertex " + str(xCoord) + " " + str(yCoord) + " " + str(zCoord))
    vertexId = cubit.get_last_id("vertex")

    return vertexId

# ===================================================================

def CreateVertexOnCurve(curveID, closeToVertexID):
    """
    Function creating a vertex on the curve specified by its curveID 
    close to a vertex specified by closeToVertexID returning 
    """

    cmd('create vertex on curve ' + str(curveID) + ' close_to vertex ' + str(closeToVertexID) )
    newVertexID = cubit.get_last_id('vertex')

    return newVertexID

# ===================================================================

def CreateAllVertices(vertexList):
    """
    Function creating vertices specified in the list 'vertexList'
    via their x, y and z coordinates and returning an extended
    vertexList 'vertexListExt' with vertex ID and coordinates in
    each list entry.
    """

    for vertex in vertexList:

        vertexId = CreateVertex(vertex[0], vertex[1], vertex[2])
        vertex.insert(0, vertexId)

# ===================================================================
# ===================================================================


def CreateLineFromVertices(vertex1, vertex2):
    """
    Function creating a line from two vertex IDs and returning
    the curve ID.
    """

    cmd("create curve vertex " + str(vertex1) + " vertex " + str(vertex2))
    curveId = cubit.get_last_id("curve")

    return curveId

# ===================================================================


def CreateArcFromVertices(vert1, vert2, vert3):
    """
    Function creating an arc defined by three vertex IDs and returning
    the curve ID.
    """

    cmd("create curve arc three vertex " + str(vert1) + " " + str(vert2) + " " + str(vert3))
    curveId = cubit.get_last_id("curve")

    return curveId

# ===================================================================


def CreateCurveFromVertices(vertex):
    """
    Function creating a curve as a line or an arc depending on how many
    vertex IDs are specified in the tuple 'vertex' (2 = line, 3 = arx).
    The ID of the created curve is returned.
    """

    if len(vertex) == 2:
        curveId = CreateLineFromVertices(vertex[0], vertex[1])
    elif len(vertex) == 3:
        curveId = CreateArcFromVertices(vertex[0], vertex[1], vertex[2])
    else:
        raise IndexError("'vertex' is only allowed to have a length of 2 or 3.")

    return curveId

# ===================================================================

def CreateCircle(radius, center, normal, startAngle=0, stopAngle=360):
    """
    Function creating a circle with 'radius at 'center' in the plane defined by 'normal'.
    If 'startAngle' and 'stopAngle' are specified, the circle can be constructed such that
    it does not close.

    Input data:
    --------------

    radius: double
            specifying radius of circle

    center: list of doubles
            [xCoord, yCoord, zCoord]

    normal: list of doubles or ints
            vector specifying normal of surface
            [xDirection, yDirection, zDirection] of normal vector

    startAngle: int or double
                value specifying start position in degree of circle

    stopAngle: int or double
               value specifying stop position in degree of circle

    Output data:
    ---------------

    curveID: int
             value specifying Cubit curve ID

    Usage:
    ---------

    curveID = CreateCircle(radius, center, normal, startAngle=0, stopAngle=360)
    """
    
    stringCenter = str(center[0]) + " " + str(center[1]) + " " + str(center[2])
    stringNormal = str(normal[0]) + " " + str(normal[1]) + " " + str(normal[2])

    cmd("create curve arc radius " + str(radius) + " center location " + stringCenter + " normal " + stringNormal + " start angle " + str(startAngle) + " stop angle " + str(stopAngle))

    curveID = cubit.get_last_id("curve")

    return curveID

# ===================================================================


def CreateAllCurves(vertexList, curveList):
    """
    Function creating curves from vertices.
    The curveID is added to every sublist in 'curveList' as first entry.
    The index of the already existing values is increased by one.

    Input data:
    --------------

    vertexList: list of lists
                each entries contains [vertexId, xCoord, yCoord, zCoord]

    curveList: list of lists
                each entry contains the position of the vertices belonging to that curve;
                the positions of the vertices specify the position in 'vertexList'


    Return data:
    ---------------

    None


    Usage:
    ---------

    CreateAllCurves(vertexList, curveList)

    """


    for curve in curveList:

        vertexIdList = list()
        for posInVertexList in curve:
            vertexIdList.append(vertexList[posInVertexList][0])

        curveId = CreateCurveFromVertices(vertexIdList)

        curve.insert(0, curveId)


# ===================================================================

def SplitCurveAtVertex(curveID, vertexID):
    """
    Function splitting a curve at a specific vertex (which lies on this curve)
    and returns the curveIDs if the two (new) parts of the curve.
    """
    
    cmd('split curve ' + str(curveID) + ' at vertex ' + str(vertexID) )
    newCurveID1 = cubit.get_last_id('curve')
    newCurveID2 = newCurveID1 - 1

    return newCurveID1, newCurveID2


# ===================================================================
# ===================================================================


def CreateGeometryGroup(groupName, entityType, entityIdList):
    """
    Function creating a geometry group named 'groupName' with entities
    of type 'entityType' specified in list 'entityIdList'.
    'entityType' can be 'vertex' or 'curve'.
    The groupId is returned. 
    """

    cmdString = 'group "' + groupName + '" add '

    if entityType == 'curve':

        cmdString = cmdString + 'curve'

    if entityType == 'vertex':

        cmdString = cmdString + 'vertex'


    for id in entityIdList:
        cmdString = cmdString + " " + str(id)

    cmd(cmdString)

    groupId = cubit.get_id_from_name(groupName)

    return groupId


# ===================================================================


def CreateGeomGroupListDict(overallVertexList, overallCurveList, entityDict):
    """
    Function creating geometry groups acoording to 'entityDict'.
    The geometry group name and its corresponding ID are included in every dict
    in 'entityDict' with keys 'geomGroupName' and 'geomGroupID'.


    Input data:
    --------------

    overallVertexList: list of lists
                       [vertexID, xCoord, yCoord, zCoord]

    overallCurveList: list of lists
                      [curveID, vertex1Pos, vertex2Pos, ...]
                      vertex position w.r.t. overallVertexList

    entityDict: list of dicts
                {"name", "curves", "vertices"}
                curves, vertices are lists specifying position w.r.t
                overallVertexList and overallCurveList resp.


    Return data:
    ---------------

    None


    Usage:
    ---------

    CreateGeomGroupListDict(overallVertexList, overallCurveList, entityDict)

    """

    for dict in entityDict:

        name          = dict["name"]
        geomGroupName = 'grGeom_%s' %(name)

        if 'curves' in dict and dict["curves"] != []:
            curveIdList  = dict["curves"]
            entityIdList = [overallCurveList[i][0] for i in curveIdList]
            entityType   = 'curve'

        if 'vertices' in dict and dict["vertices"] != []:
            vertexIdList = dict["vertices"]
            entityIdList = [overallVertexList[i][0] for i in vertexIdList]
            entityType   = 'vertex'

        groupId = CreateGeometryGroup(geomGroupName , entityType, entityIdList)
        dict.update({"geomGroupName" : geomGroupName, "geomGroupId" : groupId})


# ===================================================================
# ===================================================================


def CreateSurfFromCurves(curve):
    """
    Function generating a surface using the list 'curve' containing curve IDs and
    returning the surface ID.
    """

    cmdString = "create surface curve"
    for curveId in curve:
        cmdString = cmdString + " " + str(curveId)

    cmd(cmdString)
    surfId = cubit.get_last_id("surface")

    return surfId

# ===================================================================


def CreateRectSurfFromVertices(vertList):
    """
    Function creating a rectangular surface from a list of vertices and
    returning the surface ID.
    """

    vertACoord = vertList[0] # bottom left
    vertBCoord = vertList[1] # bottom right
    vertCCoord = vertList[2] # top    right
    vertDCoord = vertList[3] # top    left

    vertAId    = CreateVertex(vertACoord[0], vertACoord[1], vertACoord[2])
    vertBId    = CreateVertex(vertBCoord[0], vertBCoord[1], vertBCoord[2])
    vertCId    = CreateVertex(vertCCoord[0], vertCCoord[1], vertCCoord[2])
    vertDId    = CreateVertex(vertDCoord[0], vertDCoord[1], vertDCoord[2])

    curve1     = CreateLineFromVertices(vertAId, vertBId)
    curve2     = CreateLineFromVertices(vertBId, vertCId)
    curve3     = CreateLineFromVertices(vertCId, vertDId)
    curve4     = CreateLineFromVertices(vertDId, vertAId)

    surf1      = CreateSurfFromCurves(curve1, curve2, curve3, curve4)

    return surf1

# ===================================================================


def CreateCircSurfFromVertices(center, vertex1, vertex2):
    """
    Function creating circular surface using three vertex IDs (center, vertex1, vertex2)
    and returning the surface ID.
    """

    cmd("create surface circle center vertex " + str(center) + " " + str(vertex1) + " " + str(vertex2))
    surfId = cubit.get_last_id("surface")

    return surfId

# ===================================================================


def CreateAllSurfaces(curveList, surfaceListDict):
    """
    Function creating surfaces from curves.
    The surfaceID is included in every dict in surfaceListDict.


    Input data:
    --------------

    curveList: list of lists
                Each entry contains the contains [curveId, vertex1Pos, vertex2Pos, ...].
                The vertex position specify the list-position of each vertex in 'vertexList'

    surfaceListDict: list of dicts
                     Each dict contains {"surfaceNumber", "curves", "meshScheme"},
                     whereas "curves" is a list specifying the list-position of each curve belonging to this surface.
                     The list-position is defined with respect to the position in curveList.


    Return data:
    ---------------

    None


    Usage:
    ---------

    CreateAllSurfaces( curveList, surfaceListDict)

    """


    for surface in surfaceListDict:

        surfId = CreateSurfFromCurves([ curveList[posInCurveList][0] for posInCurveList in surface["curves"] ])

        surface.update({"surfaceID" : surfId})


# ===================================================================

def PartitionSurfaceUsingCurve(surfaceID, curveID):

    cmd('partition create surface ' + str(surfaceID) + ' curve ' + str(curveID))

    newSurfID = cubit.get_last_id("surface")

    return newSurfID

# ===================================================================
# ===================================================================


def SubtractBodies(remainingBody, vanishingBody):
    """
    Function subtracting two bodies from each other and
    returning the new body and surface IDs.
    """

    # body1  = cubit.get_owning_body("surface", surf1) 
    # body2  = cubit.get_owning_body("surface", surf2)
    # [body3, surf3] = SubtractBodies(body1, body2)

    cmd("subtract body " + str(vanishingBody) + " from body " + str(remainingBody))
    newSurf = cubit.get_last_id("surface")
    newBody = cubit.get_owning_body("surface", newSurf)

    return newBody, newSurf

# ===================================================================

def SubtractSurfaces(surfaceID1, surfaceID2, nSurfNew):
    """
    Function subtracting two surfaces from each other:

    'subtractID1 from surfaceID2'

    nSurfNew: int
              number of surfaces to be created by subtraction
    """

    volumeID1 = cubit.get_owning_volume("surface", surfaceID1)
    volumeID2 = cubit.get_owning_volume("surface", surfaceID2)

    cubit.cmd('subtract volume ' + str(volumeID1) + ' from volume ' + str(volumeID2) )

    newVolume  = cubit.get_last_id('volume')
    newSurface = cubit.get_relatives('volume', newVolume, 'surface')
    newSurface = newSurface[0]

    newVolumeList  = []
    newSurfaceList = []

    for counter in range(nSurfNew):
        if counter == 0:
            newVolumeList.append(newVolume)
            newSurfaceList.append(newSurface)
        else:
            previousCreatedVolume = newVolumeList[counter-1]
            createdVolume         = previousCreatedVolume - counter
            createdSurface        = cubit.get_relatives('volume', createdVolume, 'surface')
            createdSurface        = createdSurface[0]

            newVolumeList.append(createdVolume)
            newSurfaceList.append(createdSurface)

    return newVolumeList, newSurfaceList

# ===================================================================

def IsInsideBoundingBox(surfaceID1, surfaceID2):
    """
    Function checking if surface1 is inside the bounding box
    of surface2.
    """

    boundingBox1 = cubit.get_bounding_box('surface', surfaceID1)
    boundingBox2 = cubit.get_bounding_box('surface', surfaceID2)

    # surface1
    xMinSurf1 = boundingBox1[0]
    xMaxSurf1 = boundingBox1[1]

    yMinSurf1 = boundingBox1[3]
    yMaxSurf1 = boundingBox1[4]

    # surface2
    xMinSurf2 = boundingBox2[0]
    xMaxSurf2 = boundingBox2[1]

    yMinSurf2 = boundingBox2[3]
    yMaxSurf2 = boundingBox2[4]


    if xMinSurf1 > xMinSurf2 and xMinSurf1 < xMaxSurf2 and xMaxSurf1 < xMaxSurf2 and xMaxSurf1 > xMinSurf2:
        # surface1 is in x-direction inside bounding box of surface2
        isInBoundingBoxInXDirection = True
    else:
        isInBoundingBoxInXDirection = False

    if yMinSurf1 > yMinSurf2 and yMinSurf1 < yMaxSurf2 and yMaxSurf1 < yMaxSurf2 and yMaxSurf1 > yMinSurf2:
        # surface1 is in y-direction inside the bounding box of surface2
        isInBoundingBoxInYDirection = True
    else:
        isInBoundingBoxInYDirection = False

    if isInBoundingBoxInXDirection and isInBoundingBoxInYDirection:
        isInBoundingBox = True
    else:
        isInBoundingBox = False


    return isInBoundingBox

# ===================================================================

def GetEnclosedSurface(surfaceID1, surfaceID2):
    """
    Function checking if one surface enclosed the other and in case 
    it is so, return the ID of the surface which is enclosed.
    """

    surf1IsInterior = IsInsideBoundingBox(surfaceID1, surfaceID2)
    surf2IsInterior = IsInsideBoundingBox(surfaceID2, surfaceID1)

    if surf1IsInterior:
        return surfaceID1
    elif surf2IsInterior:
        return surfaceID2
    else:
        raise Exception("None of both surfaces enclose each other.")

# ===================================================================
# end | Functions
# ===================================================================