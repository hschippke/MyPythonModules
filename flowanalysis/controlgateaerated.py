# -*- coding: utf-8 -*-

"""
Module containing classes for performing flow analysis of
control gate overflow using the python api of Paraview.
"""

"""
created  by Henning Schippke
created  on 01.03.17 (based on functions created 09/2015)
modified on 21.03.17
"""

import sys

# set path to MyPythonModules (main Paraview-Python-API library)
# sys.path.append('/Users/schippke/Documents/Forschung/Programmierung/Python/MyPythonModules/')

import paraview.simple   as     pv
from   parfloan.twofluid import TwoFluidFlowAnalysis


# ===================================================================
# ===================================================================


class ControlGateFlowAnalysis(TwoFluidFlowAnalysis):
    """Flow analysis of control gate overflow.

    Methods
    -------


    Attributes
    ----------


    Example
    -------


    """

    def __init__(self, loadPath    ,
                       loadName    ,
                       savePath    ,
                       saveName    ,
                       timeInstant ,
                       description ,
                       **kwargs ):


        super(ControlGateFlowAnalysis, self).__init__(loadPath, loadName,
                                                      savePath, saveName,
                                                      timeInstant,
                                                      headingText     = "Hinterlüftetes Überfallschütz",
                                                      descriptionDict = description,
                                                      **kwargs)


    # ===============================================================
    # ===============================================================

    # ===============================================================
    # Methods
    # ===============================================================


    # ===============================================================
    # end | Methods
    # ===============================================================

# ===================================================================
# end | class ControlGateFlowAnalysis
# ===================================================================


class ControlGateFlowAnalysisOverview(ControlGateFlowAnalysis):
    """Flow analysis of control gate overflow.
        Rendering complete geometry.

    Methods
    -------


    Attributes
    ----------


    Example
    -------


    """

    def __init__(self, loadPath    ,
                       loadName    ,
                       savePath    ,
                       saveName    ,
                       timeInstant ,
                       description ,
                       **kwargs ):

        saveName = saveName + '_Overview'

        super(ControlGateFlowAnalysisOverview, self).__init__(loadPath, loadName,
                                                              savePath, saveName,
                                                              timeInstant,
                                                              description,
                                                              headingPosition = [0.365, 0.715],
                                                              **kwargs)

        # CreateView
        self.figRatio       = 2.0
        self.camPos         = [14.5, 0.51, 1.0]
        self.camFocPoint    = [14.5, 0.51, 0.0]
        self.camParScal     = 2.0

        # VelocityField
        self.scalBarPos     = [0.395, 0.24]    # Einstellung ParaView 5.3
        self.scalBarPos2    = [0.20, 0.20]    # Einstellung ParaView 5.3
        # self.scalBarPos     = [0.395, 0.23] # Einstellung ParaView 4.3.1
        # self.scalBarPos2    = [0.20 , 0.43] # Einstellung ParaView 4.3.1

        self.scalBarOrient  = 'Horizontal'

        # CreateTimeAnnotation
        self.tPos           = [ 0.85, 0.27]

        # CreateCalcExampleDescription
        self.descrpBoxPos   = [0.08, 0.20]     # Einstellung ParaView 5.3
        self.descrpBox2Pos  = [0.15, 0.20]     # Einstellung ParaView 5.3
        # self.descrpBoxPos   = [0.08, 0.20]   # Einstellung ParaView 4.3.1
        # self.descrpBox2Pos  = [0.15, 0.196]  # Einstellung ParaView 4.3.1

        self.streamlineStart = [11.5, 0.02, 0.00]
        self.streamlineEnd   = [11.5, 1.00, 0.00]


    # ===============================================================
    # ===============================================================

    # ===============================================================
    # Methods
    # ===============================================================

    def ShowVelocityWithZeroLevelSet(self, **kwargs):

        # CreateTitle
        self.subHeadPos     = [0.38, 0.67]

        super(ControlGateFlowAnalysisOverview, self).ShowVelocityWithZeroLevelSet(
                                                          figureRatio                  = self.figRatio     ,
                                                          cameraPosition               = self.camPos       ,
                                                          cameraFocalPoint             = self.camFocPoint  ,
                                                          cameraParallelScale          = self.camParScal   ,
                                                          scBrPosition                 = self.scalBarPos   ,
                                                          scBrPosition2                = self.scalBarPos2  ,
                                                          scBrOrientation              = self.scalBarOrient,
                                                          subHeadingPosition           = self.subHeadPos   ,
                                                          timePosition                 = self.tPos         ,
                                                          calcExamplDescrpBoxPosition  = self.descrpBoxPos ,
                                                          calcExamplDescrpBox2Position = self.descrpBox2Pos)



    # ===============================================================


    def ShowVelocityWithVectorsAndZeroLevelSet(self, **kwargs):

        # CreateTitle
        self.subHeadPos      = [0.345, 0.67]

        velocityVectors_numbSamplePoints = 4000
        velocityVectors_scaleFactor      = 0.25

        # velocityVectors_glypheType       = 'Arrow'
        # velocityVectors_color            = 'GlyphVector'

        velocityVectors_glypheType       = '2D Glyph'
        velocityVectors_color            = [1.0, 1.0, 1.0]
        # velocityVectors_color            = [0.1, 0.1, 0.1]


        super(ControlGateFlowAnalysisOverview, self).ShowVelocityWithVectorsAndZeroLevelSet(
                                                          velVecNumbSamplePoints       = velocityVectors_numbSamplePoints,
                                                          velVecScaleFac               = velocityVectors_scaleFactor,
                                                          velVecGlyphType              = velocityVectors_glypheType,
                                                          velVecColor                  = velocityVectors_color,
                                                          figureRatio                  = self.figRatio,
                                                          cameraPosition               = self.camPos,
                                                          cameraFocalPoint             = self.camFocPoint,
                                                          cameraParallelScale          = self.camParScal,
                                                          scBrPosition                 = self.scalBarPos,
                                                          scBrPosition2                = self.scalBarPos2,
                                                          scBrOrientation              = self.scalBarOrient,
                                                          subHeadingPosition           = self.subHeadPos,
                                                          timePosition                 = self.tPos,
                                                          calcExamplDescrpBoxPosition  = self.descrpBoxPos,
                                                          calcExamplDescrpBox2Position = self.descrpBox2Pos)



    # ===============================================================


    def ShowVelocityWithStreamlinesAndZeroLevelSet(self, **kwargs):

        # CreateTitle
        self.subHeadPos      = [0.335, 0.67]

        super(ControlGateFlowAnalysisOverview, self).ShowVelocityWithStreamlinesAndZeroLevelSet(
                                                          streamlineSeedPoint1         = self.streamlineStart,
                                                          streamlineSeedPoint2         = self.streamlineEnd,
                                                          figureRatio                  = self.figRatio,
                                                          cameraPosition               = self.camPos,
                                                          cameraFocalPoint             = self.camFocPoint,
                                                          cameraParallelScale          = self.camParScal,
                                                          scBrPosition                 = self.scalBarPos,
                                                          scBrPosition2                = self.scalBarPos2,
                                                          scBrOrientation              = self.scalBarOrient,
                                                          subHeadingPosition           = self.subHeadPos,
                                                          timePosition                 = self.tPos,
                                                          calcExamplDescrpBoxPosition  = self.descrpBoxPos,
                                                          calcExamplDescrpBox2Position = self.descrpBox2Pos)



    # ===============================================================


    def ShowVelocityWithVectorsStreamlinesAndZeroLevelSet(self, **kwargs):

        # CreateTitle
        self.subHeadPos      = [0.31, 0.67]

        super(ControlGateFlowAnalysisOverview, self).ShowVelocityWithVectorsStreamlinesAndZeroLevelSet(
                                                          streamlineSeedPoint1         = self.streamlineStart,
                                                          streamlineSeedPoint2         = self.streamlineEnd,
                                                          figureRatio                  = self.figRatio,
                                                          cameraPosition               = self.camPos,
                                                          cameraFocalPoint             = self.camFocPoint,
                                                          cameraParallelScale          = self.camParScal,
                                                          scBrPosition                 = self.scalBarPos,
                                                          scBrPosition2                = self.scalBarPos2,
                                                          scBrOrientation              = self.scalBarOrient,
                                                          subHeadingPosition           = self.subHeadPos,
                                                          timePosition                 = self.tPos,
                                                          calcExamplDescrpBoxPosition  = self.descrpBoxPos,
                                                          calcExamplDescrpBox2Position = self.descrpBox2Pos)



    # ===============================================================




    def ShowVelocityGradientWithZeroLevelSet(self, **kwargs):

        # CreateTitle
        self.subHeadPos     = [0.364, 0.67]

        super(ControlGateFlowAnalysisOverview, self).ShowVelocityGradientWithZeroLevelSet(
                                                          figureRatio                  = self.figRatio     ,
                                                          cameraPosition               = self.camPos       ,
                                                          cameraFocalPoint             = self.camFocPoint  ,
                                                          cameraParallelScale          = self.camParScal   ,
                                                          scBrPosition                 = self.scalBarPos   ,
                                                          scBrPosition2                = self.scalBarPos2  ,
                                                          scBrOrientation              = self.scalBarOrient,
                                                          subHeadingPosition           = self.subHeadPos   ,
                                                          timePosition                 = self.tPos         ,
                                                          calcExamplDescrpBoxPosition  = self.descrpBoxPos ,
                                                          calcExamplDescrpBox2Position = self.descrpBox2Pos)



    # ===============================================================


    def ShowVelocityDivergenceWithZeroLevelSet(self, **kwargs):

        # CreateTitle
        self.subHeadPos     = [0.36, 0.67]

        posTextCalcDiv      = [0.67, 0.27]

        super(ControlGateFlowAnalysisOverview, self).ShowVelocityDivergenceWithZeroLevelSet(
                                                          figureRatio                  = self.figRatio     ,
                                                          cameraPosition               = self.camPos       ,
                                                          cameraFocalPoint             = self.camFocPoint  ,
                                                          cameraParallelScale          = self.camParScal   ,
                                                          scBrPosition                 = self.scalBarPos   ,
                                                          scBrPosition2                = self.scalBarPos2  ,
                                                          scBrOrientation              = self.scalBarOrient,
                                                          subHeadingPosition           = self.subHeadPos   ,
                                                          timePosition                 = self.tPos         ,
                                                          calcExamplDescrpBoxPosition  = self.descrpBoxPos ,
                                                          calcExamplDescrpBox2Position = self.descrpBox2Pos,
                                                          calcDivPosText               = posTextCalcDiv)



    # ===============================================================


    def ShowVelocityDivergenceWithIsosurfacesAndZeroLevelSet(self, **kwargs):

        # CreateTitle
        self.subHeadPos     = [0.335, 0.67]

        posTextCalcDiv      = [0.67, 0.27]

        super(ControlGateFlowAnalysisOverview, self).ShowVelocityDivergenceWithIsosurfacesAndZeroLevelSet(
                                                          figureRatio                  = self.figRatio     ,
                                                          cameraPosition               = self.camPos       ,
                                                          cameraFocalPoint             = self.camFocPoint  ,
                                                          cameraParallelScale          = self.camParScal   ,
                                                          scBrPosition                 = self.scalBarPos   ,
                                                          scBrPosition2                = self.scalBarPos2  ,
                                                          scBrOrientation              = self.scalBarOrient,
                                                          subHeadingPosition           = self.subHeadPos   ,
                                                          timePosition                 = self.tPos         ,
                                                          calcExamplDescrpBoxPosition  = self.descrpBoxPos ,
                                                          calcExamplDescrpBox2Position = self.descrpBox2Pos,
                                                          calcDivPosText               = posTextCalcDiv)



    # ===============================================================


    def ShowVorticityFieldWithZeroLevelSet(self, **kwargs):

        # CreateTitle
        self.subHeadPos     = [0.315, 0.67]

        super(ControlGateFlowAnalysisOverview, self).ShowVorticityFieldWithZeroLevelSet(
                                                          figureRatio                  = self.figRatio     ,
                                                          cameraPosition               = self.camPos       ,
                                                          cameraFocalPoint             = self.camFocPoint  ,
                                                          cameraParallelScale          = self.camParScal   ,
                                                          scBrPosition                 = self.scalBarPos   ,
                                                          scBrPosition2                = self.scalBarPos2  ,
                                                          scBrOrientation              = self.scalBarOrient,
                                                          subHeadingPosition           = self.subHeadPos   ,
                                                          timePosition                 = self.tPos         ,
                                                          calcExamplDescrpBoxPosition  = self.descrpBoxPos ,
                                                          calcExamplDescrpBox2Position = self.descrpBox2Pos)



    # ===============================================================




    def ShowMomentumWithZeroLevelSet(self, **kwargs):

        # CreateTitle
        self.subHeadPos     = [0.405, 0.67]

        super(ControlGateFlowAnalysisOverview, self).ShowMomentumWithZeroLevelSet(
                                                          figureRatio                  = self.figRatio     ,
                                                          cameraPosition               = self.camPos       ,
                                                          cameraFocalPoint             = self.camFocPoint  ,
                                                          cameraParallelScale          = self.camParScal   ,
                                                          scBrPosition                 = self.scalBarPos   ,
                                                          scBrPosition2                = self.scalBarPos2  ,
                                                          scBrOrientation              = self.scalBarOrient,
                                                          subHeadingPosition           = self.subHeadPos   ,
                                                          timePosition                 = self.tPos         ,
                                                          calcExamplDescrpBoxPosition  = self.descrpBoxPos ,
                                                          calcExamplDescrpBox2Position = self.descrpBox2Pos)

    # ===============================================================


    def ShowEnergyWithZeroLevelSet(self, **kwargs):

        # CreateTitle
        self.subHeadPos     = [0.405, 0.67]

        super(ControlGateFlowAnalysisOverview, self).ShowEnergyWithZeroLevelSet(
                                                          figureRatio                  = self.figRatio     ,
                                                          cameraPosition               = self.camPos       ,
                                                          cameraFocalPoint             = self.camFocPoint  ,
                                                          cameraParallelScale          = self.camParScal   ,
                                                          scBrPosition                 = self.scalBarPos   ,
                                                          scBrPosition2                = self.scalBarPos2  ,
                                                          scBrOrientation              = self.scalBarOrient,
                                                          subHeadingPosition           = self.subHeadPos   ,
                                                          timePosition                 = self.tPos         ,
                                                          calcExamplDescrpBoxPosition  = self.descrpBoxPos ,
                                                          calcExamplDescrpBox2Position = self.descrpBox2Pos)

    # ===============================================================


    def ShowEnergyWithIsosurfacesAndZeroLevelSet(self, **kwargs):

        # CreateTitle
        self.subHeadPos     = [0.37, 0.67]

        super(ControlGateFlowAnalysisOverview, self).ShowEnergyWithIsosurfacesAndZeroLevelSet(
                                                          figureRatio                  = self.figRatio     ,
                                                          cameraPosition               = self.camPos       ,
                                                          cameraFocalPoint             = self.camFocPoint  ,
                                                          cameraParallelScale          = self.camParScal   ,
                                                          scBrPosition                 = self.scalBarPos   ,
                                                          scBrPosition2                = self.scalBarPos2  ,
                                                          scBrOrientation              = self.scalBarOrient,
                                                          subHeadingPosition           = self.subHeadPos   ,
                                                          timePosition                 = self.tPos         ,
                                                          calcExamplDescrpBoxPosition  = self.descrpBoxPos ,
                                                          calcExamplDescrpBox2Position = self.descrpBox2Pos)

    # ===============================================================




    def ShowPressureWithZeroLevelSet(self, **kwargs):

        # CreateTitle
        self.subHeadPos     = [0.41, 0.67]

        super(ControlGateFlowAnalysisOverview, self).ShowPressureWithZeroLevelSet(figureRatio                  = self.figRatio,
                                                                                  cameraPosition               = self.camPos,
                                                                                  cameraFocalPoint             = self.camFocPoint,
                                                                                  cameraParallelScale          = self.camParScal,
                                                                                  scBrPosition                 = self.scalBarPos,
                                                                                  scBrPosition2                = self.scalBarPos2,
                                                                                  scBrOrientation              = self.scalBarOrient,
                                                                                  subHeadingPosition           = self.subHeadPos,
                                                                                  timePosition                 = self.tPos,
                                                                                  calcExamplDescrpBoxPosition  = self.descrpBoxPos,
                                                                                  calcExamplDescrpBox2Position = self.descrpBox2Pos)

    # ===============================================================


    def ShowPressureWithIsobarsAndZeroLevelSet(self, **kwargs):

        # CreateTitle
        self.subHeadPos     = [0.375, 0.67]

        super(ControlGateFlowAnalysisOverview, self).ShowPressureWithIsobarsAndZeroLevelSet(figureRatio                  = self.figRatio,
                                                                                            cameraPosition               = self.camPos,
                                                                                            cameraFocalPoint             = self.camFocPoint,
                                                                                            cameraParallelScale          = self.camParScal,
                                                                                            scBrPosition                 = self.scalBarPos,
                                                                                            scBrPosition2                = self.scalBarPos2,
                                                                                            scBrOrientation              = self.scalBarOrient,
                                                                                            subHeadingPosition           = self.subHeadPos,
                                                                                            timePosition                 = self.tPos,
                                                                                            calcExamplDescrpBoxPosition  = self.descrpBoxPos,
                                                                                            calcExamplDescrpBox2Position = self.descrpBox2Pos)

    # ===============================================================


    def ShowPressureWithVelocityVectorsAndZeroLevelSet(self, **kwargs):

        # CreateTitle
        self.subHeadPos     = [0.33, 0.67]

        super(ControlGateFlowAnalysisOverview, self).ShowPressureWithVelocityVectorsAndZeroLevelSet(
                                                                    figureRatio                  = self.figRatio,
                                                                    cameraPosition               = self.camPos,
                                                                    cameraFocalPoint             = self.camFocPoint,
                                                                    cameraParallelScale          = self.camParScal,
                                                                    scBrPosition                 = self.scalBarPos,
                                                                    scBrPosition2                = self.scalBarPos2,
                                                                    scBrOrientation              = self.scalBarOrient,
                                                                    subHeadingPosition           = self.subHeadPos,
                                                                    timePosition                 = self.tPos,
                                                                    calcExamplDescrpBoxPosition  = self.descrpBoxPos,
                                                                    calcExamplDescrpBox2Position = self.descrpBox2Pos)

    # ===============================================================


    def ShowPressureWithStreamlinesAndZeroLevelSet(self, **kwargs):

        # CreateTitle
        self.subHeadPos     = [0.368, 0.67]

        super(ControlGateFlowAnalysisOverview, self).ShowPressureWithStreamlinesAndZeroLevelSet(
                                                                    streamlineSeedPoint1         = self.streamlineStart,
                                                                    streamlineSeedPoint2         = self.streamlineEnd,
                                                                    figureRatio                  = self.figRatio,
                                                                    cameraPosition               = self.camPos,
                                                                    cameraFocalPoint             = self.camFocPoint,
                                                                    cameraParallelScale          = self.camParScal,
                                                                    scBrPosition                 = self.scalBarPos,
                                                                    scBrPosition2                = self.scalBarPos2,
                                                                    scBrOrientation              = self.scalBarOrient,
                                                                    subHeadingPosition           = self.subHeadPos,
                                                                    timePosition                 = self.tPos,
                                                                    calcExamplDescrpBoxPosition  = self.descrpBoxPos,
                                                                    calcExamplDescrpBox2Position = self.descrpBox2Pos)

    # ===============================================================


    def ShowPressureWithVelocityVectorsStreamlinesAndZeroLevelSet(self, **kwargs):

        # CreateTitle
        self.subHeadPos     = [0.30, 0.67]

        super(ControlGateFlowAnalysisOverview, self).ShowPressureWithVelocityVectorsStreamlinesAndZeroLevelSet(
                                                                    streamlineSeedPoint1         = self.streamlineStart,
                                                                    streamlineSeedPoint2         = self.streamlineEnd,
                                                                    figureRatio                  = self.figRatio,
                                                                    cameraPosition               = self.camPos,
                                                                    cameraFocalPoint             = self.camFocPoint,
                                                                    cameraParallelScale          = self.camParScal,
                                                                    scBrPosition                 = self.scalBarPos,
                                                                    scBrPosition2                = self.scalBarPos2,
                                                                    scBrOrientation              = self.scalBarOrient,
                                                                    subHeadingPosition           = self.subHeadPos,
                                                                    timePosition                 = self.tPos,
                                                                    calcExamplDescrpBoxPosition  = self.descrpBoxPos,
                                                                    calcExamplDescrpBox2Position = self.descrpBox2Pos)

    # ===============================================================





    def ShowLevelSetWithZeroIsosurface(self, **kwargs):

        # CreateTitle
        self.subHeadPos     = [0.40, 0.67]

        super(ControlGateFlowAnalysisOverview, self).ShowLevelSetWithZeroIsosurface(figureRatio                  = self.figRatio,
                                                                                    cameraPosition               = self.camPos,
                                                                                    cameraFocalPoint             = self.camFocPoint,
                                                                                    cameraParallelScale          = self.camParScal,
                                                                                    scBrPosition                 = self.scalBarPos,
                                                                                    scBrPosition2                = self.scalBarPos2,
                                                                                    scBrOrientation              = self.scalBarOrient,
                                                                                    subHeadingPosition           = self.subHeadPos,
                                                                                    timePosition                 = self.tPos,
                                                                                    calcExamplDescrpBoxPosition  = self.descrpBoxPos,
                                                                                    calcExamplDescrpBox2Position = self.descrpBox2Pos)

    # ===============================================================


    def ShowLevelSetGradientWithZeroIsosurface(self, **kwargs):

        # CreateTitle
        self.subHeadPos     = [0.374, 0.67]

        super(ControlGateFlowAnalysisOverview, self).ShowLevelSetGradientWithZeroIsosurface(figureRatio                  = self.figRatio,
                                                                                    cameraPosition               = self.camPos,
                                                                                    cameraFocalPoint             = self.camFocPoint,
                                                                                    cameraParallelScale          = self.camParScal,
                                                                                    scBrPosition                 = self.scalBarPos,
                                                                                    scBrPosition2                = self.scalBarPos2,
                                                                                    scBrOrientation              = self.scalBarOrient,
                                                                                    subHeadingPosition           = self.subHeadPos,
                                                                                    timePosition                 = self.tPos,
                                                                                    calcExamplDescrpBoxPosition  = self.descrpBoxPos,
                                                                                    calcExamplDescrpBox2Position = self.descrpBox2Pos)

    # ===============================================================




    def ShowDensityField(self, **kwargs):

        # CreateTitle
        self.subHeadPos     = [0.46, 0.67]

        super(ControlGateFlowAnalysisOverview, self).ShowDensityField(figureRatio                  = self.figRatio,
                                                                      cameraPosition               = self.camPos,
                                                                      cameraFocalPoint             = self.camFocPoint,
                                                                      cameraParallelScale          = self.camParScal,
                                                                      scBrPosition                 = self.scalBarPos,
                                                                      scBrPosition2                = self.scalBarPos2,
                                                                      scBrOrientation              = self.scalBarOrient,
                                                                      subHeadingPosition           = self.subHeadPos,
                                                                      timePosition                 = self.tPos,
                                                                      calcExamplDescrpBoxPosition  = self.descrpBoxPos,
                                                                      calcExamplDescrpBox2Position = self.descrpBox2Pos)

    # ===============================================================




    def ShowViscosityWithZeroLevelSet(self, **kwargs):

        # CreateTitle
        self.subHeadPos     = [0.395, 0.67]

        super(ControlGateFlowAnalysisOverview, self).ShowViscosityWithZeroLevelSet(figureRatio                  = self.figRatio,
                                                                                             cameraPosition               = self.camPos,
                                                                                             cameraFocalPoint             = self.camFocPoint,
                                                                                             cameraParallelScale          = self.camParScal,
                                                                                             scBrPosition                 = self.scalBarPos,
                                                                                             scBrPosition2                = self.scalBarPos2,
                                                                                             scBrOrientation              = self.scalBarOrient,
                                                                                             subHeadingPosition           = self.subHeadPos,
                                                                                             timePosition                 = self.tPos,
                                                                                             calcExamplDescrpBoxPosition  = self.descrpBoxPos,
                                                                                             calcExamplDescrpBox2Position = self.descrpBox2Pos)

    # ===============================================================

    # ===============================================================
    # end | Methods
    # ===============================================================

# ===================================================================
# end | class ControlGateFlowAnalysisOverview
# ===================================================================


class ControlGateFlowAnalysisGateVicinity(ControlGateFlowAnalysis):
    """Flow analysis of control gate overflow.
       Rendering control gate vicinity only.

    Methods
    -------


    Attributes
    ----------


    Example
    -------


    """

    def __init__(self, loadPath    ,
                       loadName    ,
                       savePath    ,
                       saveName    ,
                       timeInstant ,
                       description ,
                       **kwargs ):

        saveName = saveName + '_Vicinity'

        super(ControlGateFlowAnalysisGateVicinity, self).__init__(loadPath, loadName,
                                                                  savePath, saveName,
                                                                  timeInstant,
                                                                  description,
                                                                  headingPosition = [0.365, 0.715], # !!! neu anzupassen !!!
                                                                  **kwargs)

        #!!! Positionen sind noch anzupassen !!!
        # # CreateView
        # self.figRatio       = 2.0
        # self.camPos         = [14.5, 0.51, 1.0]
        # self.camFocPoint    = [14.5, 0.51, 0.0]
        # self.camParScal     = 2.0
        #
        # # VelocityField
        # self.scalBarPos     = [0.395, 0.23]
        # self.scalBarPos2    = [0.20 , 0.43]
        # self.scalBarOrient  = 'Horizontal'
        #
        # # CreateTitle
        # self.subHeadPos     = [0.38, 0.67]
        #
        # # CreateTimeAnnotation
        # self.tPos           = [ 0.85, 0.27]
        #
        # # CreateCalcExampleDescription
        # self.descrpBoxPos   = [0.08, 0.20]
        # self.descrpBox2Pos  = [0.15, 0.196]

    # ===============================================================
    # ===============================================================

    # ===============================================================
    # Methods
    # ===============================================================

    # ===============================================================
    # end | Methods
    # ===============================================================

# ===================================================================
# end | class ControlGateFlowAnalysisGateVicinity
# ===================================================================
