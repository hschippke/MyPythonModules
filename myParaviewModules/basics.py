# -*- coding: utf-8 -*-

"""
Module containing basic Paraview commands.
"""

"""
created  by Henning Schippke
created  on 30.10.15
modified on 18.03.17
"""

import numpy             as     np
import paraview.simple   as     pv

# ===================================================================
# Functions
# ===================================================================


def LoadXDMFData(loadPath, loadName):

    # load data from xdmf file (create new xmdf reader)
    data = pv.XDMFReader(FileNames=loadPath + loadName)

    # get animation scene
    animationScene = pv.GetAnimationScene()

    return data, animationScene

# ===================================================================


def SetRenderViewSettings(view                ,
                          intActMode    = '2D',
                          bgColor       = [1.0, 1.0, 1.0],
                          viewSize      = None,
                          camPos        = None,
                          camFocPoint   = None,
                          camClipRange  = None,
                          camParScal    = None,
                          rotationCenter= [0.0, 0.0, 0.0],
                          axesCenterVis = 0,
                          axesOrientVis = 0,
                          useLight      = 0,
                          lightSwitch   = 1):
    """
    Function setting basic render view properties.
    """


    view.InteractionMode           = intActMode
    view.Background                = bgColor


    if viewSize is not None:
        view.ViewSize              = viewSize


    if camPos is not None:
        view.CameraPosition        = camPos

    # Punkt, auf den die Kamera schaut,
    # Sind die ersten beiden Werte von FocPoint und Pos in 2D gleich, so
    # wird gerade auf das Objekt geschaut. Der Abstand  betraegt die Differenz
    # des dritten Wertes.
    if camFocPoint is not None:
        view.CameraFocalPoint      = camFocPoint

    if camClipRange is not None:
        view.CameraClippingRange   = camClipRange # Setzt den Entfernungsbereich, in dem Objekte dargestellt werden.

    if camParScal is not None:
        view.CameraParallelScale   = camParScal # Verkleinern des Objektes in der Ausgabe


    view.CenterOfRotation          = rotationCenter


    view.CenterAxesVisibility      = axesCenterVis
    view.OrientationAxesVisibility = axesOrientVis


    # Brighter colors are gained by switching of the "light kid" (useLight= 0)
    # in combination with switching on the "head light" (lightSwitch = 1).
    # If only the "light kid" is switch off, all displayed objects remain dark.
    view.UseLight    = useLight    # light kit
    view.LightSwitch = lightSwitch # head light

# ===================================================================


def SetTextBoxDisplay(textboxDisp                        ,
                      windowLocation    = 'AnyLocation'  ,
                      position          = [0.05, 0.05]   ,
                      position2         = None           ,
                      fontFamily        = 'Times'        ,
                      fontSize          = '10'           ,
                      fontBold          = 0              ,
                      fontItalic        = 0              ,
                      textColor         = [0.0, 0.0, 0.0],
                      textOpacity       = 1.0            ,
                      textShadow        = 0              ,
                      textJustification = 'Left'         ,
                      textOrientation   = 0.0            ,
                      visibility        = 1              ):
    """
    Function adjusting the display representation of a text box.
    """


    textboxDisp.WindowLocation = windowLocation
    textboxDisp.Position       = position
    if position2 is not None:
        textboxDisp.Position2  = position2

    textboxDisp.FontFamily     = fontFamily
    textboxDisp.FontSize       = fontSize

    textboxDisp.Bold           = fontBold
    textboxDisp.Italic         = fontItalic

    textboxDisp.Color          = textColor
    textboxDisp.Opacity        = textOpacity
    textboxDisp.Shadow         = textShadow

    textboxDisp.Justification  = textJustification
    textboxDisp.Orientation    = textOrientation

    textboxDisp.Visibility     = visibility


    # Properties not used:
    #
    # TextScaleMode :: default : 'Viewport'
    #
    # Interactivity :: default : 1
    #     Set whether user should be able to interactively place the text in the view.
    #
    # Input         :: default : textbox object <- pv.Show(textbox, renderView)
    #
    # Resizable     :: default : 0

# ===================================================================


def SetScalarBarSettings( scBr                                   ,
                          visibility            = 1              ,
                          position              = None           ,
                          position2             = None           ,
                          aspectRatio           = None           ,
                          orientation           = None           ,
                          lookupTable           = None           ,
                          title                 = ''             ,
                          title2                = ''             ,
                          titleFontFamily       = 'Times'        ,
                          titleFontSize         = 8              ,
                          titleBold             = 0              ,
                          titleItalic           = 0              ,
                          titleColor            = [0.0, 0.0, 0.0],
                          titleOpacity          = 1.0            ,
                          titleShadow           = 0              ,
                          titleJustification    = 'Centered'     ,
                          labelFormatAuto       = 0              ,
                          labelRangeAdd         = 1              ,
                          labelRangeFormat      = None           ,
                          labelDrawTick         = 1              ,
                          labelNumberOf         = None           ,
                          labelFormat           = None           ,
                          labelFontFamily       = 'Times'        ,
                          labelFontSize         = 8              ,
                          labelBold             = 0              ,
                          labelItalic           = 0              ,
                          labelColor            = [0.0, 0.0, 0.0],
                          labelOpacity          = 1.0            ,
                          labelShadow           = 0              ,
                          labelDrawTickMarks    = 1              ,
                          labelDrawSubTickMarks = 0              ,
                          labelTextPosition     = None           ,
                          annotationDraw        = 0              ,
                          annotationAddRang     = 0              ,
                          annotationAutomatic   = 0              ,
                          annotationNanDraw     = 0              ,
                          annotationNan         = 'NaN'          ):
    """
    Function adjusting the color bar representation.
    """


    # ---------------------------------------------------------------
    # Allgemeine Einstellungen
    # ---------------------------------------------------------------

    scBr.Visibility   = visibility

    if position is not None:
        scBr.Position = position

    if position2 is not None:
        scBr.Position2   = position2
        #     position2 = [Breite, Hoehe]
        #     Breite hat nur einen Einfluss bei Orientation = 'Horitontal'
        #     Hohe   hat nur einen Einfluss bei Orientation = 'Vertical'

    if aspectRatio is not None:
        scBr.AspectRatio = aspectRatio

    if orientation is not None:
        scBr.Orientation = orientation

    if lookupTable is not None:
        scBr.LookupTable     = lookupTable

    # ---------------------------------------------------------------
    # end | Allgemein Einstellungen
    # ---------------------------------------------------------------


    # ---------------------------------------------------------------
    # Titel Einstellungen
    # ---------------------------------------------------------------

    scBr.Title              = title
    scBr.ComponentTitle     = title2

    scBr.TitleFontFamily    = titleFontFamily
    scBr.TitleFontSize      = titleFontSize

    scBr.TitleBold          = titleBold
    scBr.TitleItalic        = titleItalic

    scBr.TitleColor         = titleColor
    scBr.TitleOpacity       = titleOpacity
    scBr.TitleShadow        = titleShadow

    scBr.TitleJustification = titleJustification

    # ---------------------------------------------------------------
    # end | Titel EInstellunge
    # ---------------------------------------------------------------


    # ---------------------------------------------------------------
    # Labels Einstellungen
    # ---------------------------------------------------------------

    # labels are displayed on the right side of the color bar

    scBr.AutomaticLabelFormat     = labelFormatAuto


    scBr.AddRangeLabels           = labelRangeAdd
    if labelRangeFormat is not None:
        scBr.RangeLabelFormat     = labelRangeFormat


    scBr.DrawTickLabels           = labelDrawTick # draw color legend labels for tick marks

    if labelNumberOf is not None:
        scBr.NumberOfLabels       = labelNumberOf
    if labelFormat is not None:
        scBr.LabelFormat          = labelFormat

    scBr.LabelFontFamily          = labelFontFamily
    scBr.LabelFontSize            = labelFontSize
    scBr.LabelBold                = labelBold
    scBr.LabelItalic              = labelItalic

    scBr.LabelColor               = labelColor
    scBr.LabelOpacity             = labelOpacity
    scBr.LabelShadow              = labelShadow

    scBr.DrawTickMarks            = labelDrawTickMarks
    scBr.DrawSubTickMarks         = labelDrawSubTickMarks
    if labelTextPosition is not None:
        scBr.labelTextPosition    = labelTextPosition # Determines where the tick labels are drawn

    # ---------------------------------------------------------------
    # end | Labels Einstellungen
    # ---------------------------------------------------------------


    # ---------------------------------------------------------------
    # Annotations Einstellungen
    # ---------------------------------------------------------------

    # annotations are displayed on the left side of the color bar

    scBr.DrawAnnotations      = annotationDraw

    scBr.AddRangeAnnotations  = annotationAddRang
    scBr.AutomaticAnnotations = annotationAutomatic # if set, annotations are automatically created according the number of colors shown on the scalar bar.

    scBr.DrawNanAnnotation    = annotationNanDraw   # displays color field showing NaN color
    scBr.NanAnnotation        = annotationNan       # adds NaN annotation to color field
                                                    # in order to show this annotation
                                                    # annotationdDraw = 1

    # ---------------------------------------------------------------
    # end | Annotations Einstellungen
    # ---------------------------------------------------------------


    # ---------------------------------------------------------------
    # Properties not used
    # ---------------------------------------------------------------

    # Enabled
    #     Enable/Disable widget interaction.
    # LockPosition
    #     Set this to 1 if you don't want the application to change the scalar bar position
    #     when placing automatically.
    # Repositionable
    # Resizable
    # Selectable
    # UseNonCompositedRenderer

    # ---------------------------------------------------------------
    # end | Properties not used
    # ---------------------------------------------------------------


# ===================================================================


def PlayAnimation(animationScene, timeStep, startTime=None, endTime=None):
    """
    Function displaying snapshots of the given <animationScene> via the specified
    <timeStep>. In case no <startTime> and no <endTime> is given explicitely, the
    animation is displayed from beginning to end.
    """

    # set first time instant as start time if no explicit start time is defined
    if startTime is None:
        animationScene.GoToFirst()
        startTime = animationScene.AnimationTime

    # set last time instant as end time if no explicit end time is defined
    if endTime is None:
        animationScene.GoToLast()
        endTime = animationScene.AnimationTime

    animationScene.AnimationTime = startTime


    # show every time instant using defined timeStep size
    while animationScene.AnimationTime < endTime:

        if animationScene.AnimationTime+timeStep > endTime:
            break
        else:
            animationScene.AnimationTime += timeStep

# ===================================================================


def GetRangeOfPointDataField(dataSource, nameOfDataField):

    data           = dataSource.GetDataInformation()
    dataInfo       = data.DataInformation
    dataInfoPoint  = dataInfo.GetPointDataInformation()

    dataArray      = dataInfoPoint.GetArrayInformation(nameOfDataField)
    dataArrayRange = dataArray.GetComponentRange(0)

    # Alternatives Vorgehen:
    # pointData      = dataSource.PointData
    # dataArrayRange = pointData.GetArray(nameOfDataField).GetRange()


    dataMin        = dataArrayRange[0]
    dataMax        = dataArrayRange[1]

    return dataMin, dataMax

# ===================================================================


def IntegrateVariables(dataSource, nameOfDataField):

    integratedData  = pv.IntegrateVariables(Input=dataSource)

    # pd              = integratedData.PointData
    dataArray       = integratedData.PointData.GetArray(nameOfDataField)
    dataRange       = dataArray.GetRange()
    integratedValue = dataRange[0]

    return integratedValue

# ===================================================================


def CreateColorMapTable(data, dataField, cMapType='parula', cMapOrdering = 'normal'):

    minValue, maxValue = GetRangeOfPointDataField(data, dataField)

    if cMapType == 'parula':

        colorValues         = GetParulaColorMapValues()
        nColorValues, nCol  = colorValues.shape


    if cMapOrdering == 'inverse':
        colorValues = colorValues[::-1]
    else: # cMapOrdering = 'normal'
        pass

    cMapValues        = np.linspace(minValue, maxValue, nColorValues)

    cMap              = np.column_stack((cMapValues, colorValues))
    cMapList          = cMap.tolist()
    cMapListFlattened = [item for sublist in cMapList for item in sublist]


    return cMapListFlattened


# ===================================================================


def GetParulaColorMapValues():

    parula = np.array([[ 0.2081   ,  0.1663   ,  0.5292   ],
                       [ 0.21162  ,  0.18978  ,  0.57768  ],
                       [ 0.21225  ,  0.21377  ,  0.62697  ],
                       [ 0.2081   ,  0.2386   ,  0.67709  ],
                       [ 0.1959   ,  0.26446  ,  0.7279   ],
                       [ 0.17073  ,  0.29194  ,  0.77925  ],
                       [ 0.12527  ,  0.32424  ,  0.83027  ],
                       [ 0.059133 ,  0.35983  ,  0.86833  ],
                       [ 0.011695 ,  0.38751  ,  0.88196  ],
                       [ 0.0059571,  0.40861  ,  0.88284  ],
                       [ 0.016514 ,  0.4266   ,  0.87863  ],
                       [ 0.032852 ,  0.44304  ,  0.87196  ],
                       [ 0.049814 ,  0.45857  ,  0.86406  ],
                       [ 0.062933 ,  0.47369  ,  0.85544  ],
                       [ 0.072267 ,  0.48867  ,  0.8467   ],
                       [ 0.077943 ,  0.50399  ,  0.83837  ],
                       [ 0.079348 ,  0.52002  ,  0.83118  ],
                       [ 0.074943 ,  0.53754  ,  0.82627  ],
                       [ 0.064057 ,  0.55699  ,  0.82396  ],
                       [ 0.048771 ,  0.57722  ,  0.82283  ],
                       [ 0.034343 ,  0.59658  ,  0.81985  ],
                       [ 0.0265   ,  0.6137   ,  0.8135   ],
                       [ 0.02389  ,  0.62866  ,  0.80376  ],
                       [ 0.02309  ,  0.64179  ,  0.79127  ],
                       [ 0.022771 ,  0.65349  ,  0.77676  ],
                       [ 0.026662 ,  0.6642   ,  0.76072  ],
                       [ 0.038371 ,  0.67427  ,  0.74355  ],
                       [ 0.058971 ,  0.68376  ,  0.72539  ],
                       [ 0.0843   ,  0.69283  ,  0.70617  ],
                       [ 0.1133   ,  0.7015   ,  0.68586  ],
                       [ 0.14527  ,  0.70976  ,  0.66463  ],
                       [ 0.18013  ,  0.71766  ,  0.64243  ],
                       [ 0.21783  ,  0.72504  ,  0.61926  ],
                       [ 0.25864  ,  0.73171  ,  0.59543  ],
                       [ 0.30217  ,  0.7376   ,  0.57119  ],
                       [ 0.34817  ,  0.74243  ,  0.54727  ],
                       [ 0.39526  ,  0.7459   ,  0.52444  ],
                       [ 0.44201  ,  0.74808  ,  0.50331  ],
                       [ 0.48712  ,  0.74906  ,  0.48398  ],
                       [ 0.53003  ,  0.74911  ,  0.46611  ],
                       [ 0.57086  ,  0.74852  ,  0.44939  ],
                       [ 0.60985  ,  0.74731  ,  0.43369  ],
                       [ 0.6473   ,  0.7456   ,  0.4188   ],
                       [ 0.68342  ,  0.74348  ,  0.40443  ],
                       [ 0.71841  ,  0.74113  ,  0.39048  ],
                       [ 0.75249  ,  0.7384   ,  0.37681  ],
                       [ 0.78584  ,  0.73557  ,  0.36327  ],
                       [ 0.8185   ,  0.73273  ,  0.34979  ],
                       [ 0.85066  ,  0.7299   ,  0.33603  ],
                       [ 0.88243  ,  0.72743  ,  0.3217   ],
                       [ 0.91393  ,  0.72579  ,  0.30628  ],
                       [ 0.94496  ,  0.72611  ,  0.28864  ],
                       [ 0.9739   ,  0.7314   ,  0.26665  ],
                       [ 0.99377  ,  0.74546  ,  0.24035  ],
                       [ 0.99904  ,  0.76531  ,  0.21641  ],
                       [ 0.99553  ,  0.78606  ,  0.19665  ],
                       [ 0.988    ,  0.8066   ,  0.17937  ],
                       [ 0.97886  ,  0.82714  ,  0.16331  ],
                       [ 0.9697   ,  0.84814  ,  0.14745  ],
                       [ 0.96259  ,  0.87051  ,  0.1309   ],
                       [ 0.95887  ,  0.8949   ,  0.11324  ],
                       [ 0.95982  ,  0.92183  ,  0.094838 ],
                       [ 0.9661   ,  0.95144  ,  0.075533 ],
                       [ 0.9763   ,  0.9831   ,  0.0538   ]])

    return parula

# ===================================================================
# end | Functions
# ===================================================================
